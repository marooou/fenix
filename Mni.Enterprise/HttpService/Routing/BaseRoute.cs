﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.HttpService.Routing
{
    public class BaseRoute
    {
        public string Template { get; private set; }
        public object Constraints { get; private set; }

        public BaseRoute(string template)
        {
            this.Template = template;
        }

        public BaseRoute(string template, object constraints)
        {
            this.Template = template;
            this.Constraints = constraints;
        }
    }
}
