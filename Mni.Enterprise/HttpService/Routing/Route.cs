﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.HttpService.Routing
{
    public sealed class Route : BaseRoute
    {
        public string Name { get; private set; }
        public object Defaults { get; private set; }

        public Route(string name, string template)
            : base(template, null)
        {
            this.Name = name;
        }

        public Route(string name, string template, object defaults)
            : base(template, null)
        {
            this.Name = name;
            this.Defaults = defaults;
        }

        public Route(string name, string template, object defaults, object constraints)
            : base(template, constraints)
        {
            this.Name = name;
            this.Defaults = defaults;
        }
    }
}
