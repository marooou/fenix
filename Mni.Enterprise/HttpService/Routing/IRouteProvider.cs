﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.HttpService.Routing
{
    public interface IRouteProvider
    {
        ICollection<Route> Routes { get; }

        ICollection<BaseRoute> IgnoredRoutes { get; }

        bool AttributeDriven { get; }

        void Initialize();
    }
}
