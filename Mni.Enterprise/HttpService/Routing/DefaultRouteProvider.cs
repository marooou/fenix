﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.HttpService.Routing
{
    using System.Collections.ObjectModel;

    public class DefaultRouteProvider : IRouteProvider
    {
        public ICollection<Route> Routes { get; private set; }

        public ICollection<BaseRoute> IgnoredRoutes { get; private set; }

        public bool AttributeDriven { get; protected set; }

        internal protected DefaultRouteProvider()
        {
            this.Routes = new Collection<Route>();
            this.IgnoredRoutes = new Collection<BaseRoute>();
            this.ApplyDefaults();
        }

        public virtual void Initialize()
        {
        }

        private void ApplyDefaults()
        {
            this.AttributeDriven = true;
            this.IgnoredRoutes.Add(new BaseRoute("{resource}.axd/{*pathInfo}"));
            this.IgnoredRoutes.Add(new BaseRoute("Content/{*pathInfo}"));
            this.IgnoredRoutes.Add(new BaseRoute("Scripts/{*pathInfo}"));
        }
    }
}
