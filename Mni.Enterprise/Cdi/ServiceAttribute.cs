﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.Cdi
{
    using Mni.Core;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ServiceAttribute : ComponentAttribute
    {
        public ServiceAttribute()
        {
            this.Activation = ComponentActivation.Eager;
            this.Scope = ComponentScope.Singleton;
        }
    }
}
