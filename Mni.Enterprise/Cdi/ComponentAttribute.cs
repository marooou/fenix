﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.Cdi
{
    using Mni.Core;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public abstract class ComponentAttribute : Attribute
    {
        public Type[] Types { get; set; }
        public ComponentScope Scope { get; set; }
        public ComponentActivation Activation { get; set; }

        public IDictionary<string, object> ToDictionary()
        {
            return new Dictionary<string, object>
                {
                    { Constants.Component.Activation, this.Activation },
                    { Constants.Component.Scope, this.Scope }
                };
        }
    }
}
