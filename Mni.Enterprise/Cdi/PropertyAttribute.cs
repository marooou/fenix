﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Enterprise.Cdi
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class PropertyAttribute : Attribute
    {
        public string Key { get; private set; }
        public object Value { get; private set; }

        public PropertyAttribute(string key, object value)
        {
            this.Key = key;
            this.Value = value;
        }

        protected PropertyAttribute(string key)
        {
            this.Key = key;
            this.Value = this;
        }

        protected PropertyAttribute()
        {
            var fullName = this.GetType().FullName;
            this.Key = fullName;
            this.Value = this;
        }
    }
}
