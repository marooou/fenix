﻿
namespace Fenix.Modules.Console
{
    using System;

    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Dependency]
    internal class DefaultConsoleDecorator : IConsoleDecorator
    {
        private const string Logo = @"
   _____          _      
  |  ___|__ _ __ (_)_  __
  | |_ / _ \ '_ \| \ \/ /
  |  _|  __/ | | | |>  < 
  |_|  \___|_| |_|_/_/\_\";

        private const string TextBelowLogo = @"
 Hit '<tab>' for a list of available commands
 and '[cmd] --help' for help on a specific command.
 Hit '<ctrl-c>' or 'framework:shutdown' to shutdown Fenix.";

        private readonly IModuleContext _context;

        public DefaultConsoleDecorator(IModuleContext context)
        {
            this._context = context;
        }

        public string WelcomeMessage 
        { 
            get
            {
                return string.Format("{0}{1}\t\t   v{2}{1}{1}{3}{1}", Logo, Environment.NewLine, this.GetFrameworkVersion(), TextBelowLogo);
            }
        }

        public string Prompt
        {
            get
            {
                return "fenix>";
            }
        }

        private string GetFrameworkVersion()
        {
            IModule frameworkModule = this._context.GetModule(0);
            return frameworkModule.Manifest.Version.ToString(3);
        }
    }
}
