﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console
{
    using System.Threading;

    using Mni.Core;

    public class ConsoleActivator : IModuleActivator
    {
        private IModuleContext _context;
        private Thread _consoleThread;
        private ContainerConsole _console;

        public void Start(IModuleContext context)
        {
            this._context = context;
            this._consoleThread = new Thread(this.Run);
            this._consoleThread.Start();
        }

        private void Run()
        {
            this._console = (ContainerConsole)this._context.Resolve(typeof(ContainerConsole));
            this._console.Run();
        }

        public void Stop(IModuleContext context)
        {
            this._context = null;
            try
            {
                this._console.Exit();
                this._consoleThread.Abort();
            }
            catch
            {
            }

            this._consoleThread = null;
        }
    }
}
