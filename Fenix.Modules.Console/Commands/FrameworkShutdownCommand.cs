﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;
    using Mni.Core.Util;
    using Mni.Enterprise.Cdi;

    [Command(Scope = "framework", Name = "shutdown")]
    internal class FrameworkShutdownCommand : BaseCommand, ICommand
    {
        public FrameworkShutdownCommand(IModuleContext context)   
            : base(context)
        {
        }

        public object Execute()
        {
            WriteLine(string.Empty);
            WriteLine("Shutting down...");
            this.Context.GetFramework().Stop();
            return null;
        }
    }
}
