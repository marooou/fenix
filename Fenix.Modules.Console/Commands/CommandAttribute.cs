﻿using System;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Enterprise.Cdi;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CommandAttribute : PropertyAttribute
    {
        public const string PropertyKey = "Console.Command";

        public string Scope { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public CommandAttribute()
            : base(PropertyKey)
        {
        }

        public override string ToString()
        {
            return string.Format("[Scope: {0}, Name: {1}, Description: {2}]", this.Scope, this.Name, this.Description);
        }
    }
}
