﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using System.Collections;

    using Mni.Core;
    using Mni.Core.Util;
    using Mni.Enterprise.Cdi;

    [Command(Scope = "service", Name = "list")]
    internal class ServiceListCommand : BaseCommand, ICommand
    {
        public ServiceListCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            WriteHeader(" {0,-40}{1,-50}{2,-15}{3,10}", "Publisher", "ObjectClass", "Scope", "Activation");
            IEnumerable<IModule> modules = this.Context.GetModules();
            foreach (var module in modules)
            {
                IEnumerable<IServiceReference> svcRefs = module.GetRegisteredServices();
                foreach (var svcRef in svcRefs)
                {
                    var objectClass = svcRef.GetObjectClass();
                    WriteLine(
                        " {0,-40}{1,-50}{2,-15}{3,10}", 
                        string.Format("{0}[{1}]", module.Manifest.SymbolicName, module.Id),
                        string.Join(",", objectClass),
                        svcRef.GetScope(),
                        svcRef.GetActivation());
                }
            }

            return null;
        }
    }
}
