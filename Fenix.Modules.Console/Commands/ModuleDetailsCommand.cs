﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Command(Scope = "module", Name = "details")]
    internal class ModuleDetailsCommand : BaseCommand, ICommand
    {
        [Parameter(Index = 0)]
        public int ModuleId { get; set; }

        public ModuleDetailsCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            IModule module = this.Context.GetModule(this.ModuleId);
            WriteHeader("Details");
            WriteLine("{0}{1}", "ID", module.Id);
            WriteLine("{0}{1}", "Location", module.Location);
            WriteLine("{0}{1}", "State", module.State);

            WriteHeader("Registered services");
            IEnumerable<IServiceReference> registeredServices = module.GetRegisteredServices();

            WriteHeader("Services in use");
            IEnumerable<IServiceReference> servicesInUse = module.GetServicesInUse();
            return null;
        }
    }
}
