﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Command(Scope = "module", Name = "install")]
    internal class ModuleInstallCommand : BaseCommand, ICommand
    {
        [Parameter(Index = 0)]
        public string Path { get; set; }

        public ModuleInstallCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            this.Context.InstallModule(this.Path);
            return null;
        }
    }
}
