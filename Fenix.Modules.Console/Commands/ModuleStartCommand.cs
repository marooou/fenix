﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Command(Scope = "module", Name = "start")]
    internal class ModuleStartCommand : BaseCommand, ICommand
    {
        [Parameter(Index = 0)]
        public int ModuleId { get; set; }

        public ModuleStartCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            this.Context.GetModule(this.ModuleId).Start();
            return null;
        }
    }
}
