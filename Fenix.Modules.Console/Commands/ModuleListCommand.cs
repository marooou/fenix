﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Command(Scope = "module", Name = "list")]
    internal class ModuleListCommand : BaseCommand, ICommand
    {
        public ModuleListCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            IEnumerable<IModule> modules = this.Context.GetModules();

            System.Console.BackgroundColor = ConsoleColor.White;
            System.Console.ForegroundColor = ConsoleColor.Black;
            System.Console.WriteLine(" {0,-5}{1,-30}{2,-10}{3,15}", "ID", "SymbolicName", "Version", "State");
            System.Console.ResetColor();

            foreach (var module in modules)
            {
                if (module.Id == 0)
                {
                    continue;
                }

                System.Console.WriteLine(" {0,-5}{1,-30}{2,-10}{3,15}", module.Id, module.Manifest.SymbolicName, module.Manifest.Version, module.State);
            }

            return null;
        }
    }
}
