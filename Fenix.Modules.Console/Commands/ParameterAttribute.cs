﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ParameterAttribute : Attribute
    {
        public bool Required { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Index { get; set; }

        public ParameterAttribute()
        {
            Required = true;
            Index = -1;
        }
    }
}
