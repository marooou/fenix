﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;

    using Mni.Enterprise.Cdi;

    [Command(Scope = "module", Name = "uninstall")]
    internal class ModuleUninstallCommand : BaseCommand, ICommand
    {
        [Parameter(Index = 0)]
        public int ModuleId { get; set; }

        public ModuleUninstallCommand(IModuleContext context)
            : base(context)
        {
        }

        public object Execute()
        {
            this.Context.GetModule(this.ModuleId).Uninstall();
            return null;
        }
    }
}
