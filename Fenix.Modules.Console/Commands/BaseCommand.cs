﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console.Commands
{
    using Mni.Core;
    using Mni.Enterprise.Cdi;

    [Service(Scope = ComponentScope.Module)]
    internal abstract class BaseCommand
    {
        protected IModuleContext Context { get; private set; }

        protected BaseCommand(IModuleContext context)
        {
            this.Context = context;
        }

        protected static void WriteHeader(string format, params object[] args)
        {
            System.Console.BackgroundColor = ConsoleColor.Gray;
            System.Console.ForegroundColor = ConsoleColor.Black;
            System.Console.WriteLine(format, args);
            System.Console.ResetColor();
        }

        protected static void WriteLine(string format, params object[] args)
        {
            System.Console.WriteLine(format, args);
        }
    }
}
