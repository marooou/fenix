﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console
{
    public interface ICommand
    {
        object Execute();
    }
}
