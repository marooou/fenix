﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Modules.Console
{
    using Fenix.Modules.Console.Commands;

    using Mni.Core;
    using Mni.Core.Util;
    using Mni.Enterprise.Cdi;

    [Dependency]
    internal class ContainerConsole
    {
        private readonly IConsoleDecorator _decorator;
        private readonly IModuleContext _context;
        private bool _running;

        public ContainerConsole(IModuleContext context, IConsoleDecorator decorator)
        {
            this._decorator = decorator;
            this._context = context;
        }

        public void Run()
        {
            if (this._running)
            {
                return;
            }

            this._running = true;

            System.Console.WriteLine(this._decorator.WelcomeMessage);
            System.Console.CancelKeyPress += this.ConsoleCancelKeyPress;
            while (this._running)
            {
                System.Console.Write(this._decorator.Prompt);
                var commandStr = System.Console.ReadLine();
                if (string.IsNullOrWhiteSpace(commandStr))
                {
                    continue;
                }

                var commandReference = this.GetCommandReference(commandStr);
                if (commandReference != null)
                {
                    try
                    {
                        var command = this._context.GetService<ICommand>(commandReference);
                        command.Execute();
                        this._context.UngetService(commandReference, command);
                    }
                    catch (Exception ex)
                    {
                        // TODO: log command errors
                        System.Console.Error.WriteLine(ex);
                    }
                }
                else
                {
                    System.Console.WriteLine("Command not found: {0}", commandStr);
                }
            }
        }

        public void Exit()
        {
            this._running = false;
        }

        private void ConsoleCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            var exitCommand = new FrameworkShutdownCommand(this._context);
            exitCommand.Execute();
            System.Console.CancelKeyPress -= ConsoleCancelKeyPress;
        }

        private IServiceReference GetCommandReference(string command)
        {
            if (command == null)
            {
                return null;
            }

            string[] tokens = command.Split(':');
            string scope = null;
            string commandName;
            if (tokens.Length == 1)
            {
                commandName = tokens[0];
            }
            else if (tokens.Length == 2)
            {
                scope = tokens[0];
                commandName = tokens[1];
            }
            else
            {
                return null;
            }

            return this.FindCommandService(scope, commandName);
        }

        public IServiceReference FindCommandService(string scope, string commandName)
        {
            return this._context.GetServiceReferences<ICommand>()
                .WithProperty<CommandAttribute>(CommandAttribute.PropertyKey, c => c.Scope == scope && c.Name == commandName)
                .FirstOrDefault();
        }
    }
}
