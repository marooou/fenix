﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atsi.Web.Container.IISHost
{
    using Fenix.Framework;
    using Fenix.Framework.Core;

    using Mni.Core;

    public static class App
    {
        public static void Main(string[] args)
        {
            IFrameworkFactory factory = new FrameworkFactory();
            IFramework framework = factory.NewFramework();
            framework.Start();

            IModule console = framework.Context.InstallModule(@"D:\Fenix\Fenix.Modules.Console\");
            console.Start();

            framework.WaitForStop();
        }
    }
}
