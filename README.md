# Goals of this project. #

The goal of this project is to provide a specification and a reference implementation of a modular framework for .NET platform.

The idea is based on the OSGi spec which can be found at http://www.osgi.org/Specifications/HomePage

## Behind the courtains  ##

Let's face it. Neither Java nor .NET were built around the idea of modularity in the first place.
Personally, I blame JIT compilation and Garbage Collection.
Don't get me wrong. These are really brilliant ideas which must have saved thousands of years of coding by now,
but this is also why there is no way to 

* unload classes on Java platform other than destroying the class loader itself. (cf. http://stackoverflow.com/questions/148681/unloading-classes-in-java)
* unload assemblies on .NET platform other than unloading the whole AppDomain. (cf. http://blogs.msdn.com/b/jasonz/archive/2004/05/31/145105.aspx)

Nontheless, these are just colateral damages as far as time-saving is concerned.

I've been interested in modular technologies on Java platform for quite some time now as 
pluggable architectures are far more mature on this platform than on .NET. Quite frankly, there
is an honest reason for this superiority - class loaders.

Let's see what are the most important features of class loaders:

- **They can operate in the very same instance of the JVM.** - this prevents pieces of code to communicate accross processes using any kind of IPC like RMI, CORBA and so on.
- They are hirerarchical (a notion of parent class loader) - this creates an oportunity to delegate loading capabilities
- They have an API which is a facade for class loading.

In juxtoposition with .NET, AppDomains are slightly different:

- **They are infact separate processes** - this requires an IPC like .NET remoting or WCF
- There is no hierarchy (counting out the idea of so called domain neutral assemblies, which can be shared accross AppDomains)
- They have an API which is rather a fallback mechanism than a real facade for assembly loading (cf. AppDomain.AssemblyResolve, AppDomain.TypeResolve)

Having that in mind, AppDomains are ideologically closer to multiple instances of the JVM than class loaders.

So here we are. 
We cannot implement the OSGi spec on .NET platform because it doesn't make much sense.
But we can definitely create a new, .NET compliant, spec for modular systems.
The idea stays the same, the resources change.

I deeply believe there is yet another thing coming.
Other than IoC frameworks like Autofac, StructureMap, Unity or MEF.
More robust than PRISM or ASP.NET with its assembly shadow-copying mechanism.
And guess what? I also believe it's the Fenix project.

Especially when the .NET world opens an ocean of opportunities in front of us every day.
Just look at Nuget packaging, the OWIN spec, the ASP.NET Identity and so forth.
This opportunities are more modular than ever.
It would be a waste not to use them.

## How this project is organized ##

There is one major solution called Fenix under which there are several projects:

* Those starting with **Mni** (which stands for Modular .NET initiative) represent the *specification*
* Those starting with **Fenix** represent a *reference implementation* of the Mni spec.

At this point we can think of the Mni as the public API
and the Fenix as an implementation of this API.

Those two will shortly be separated into two repositories so that they can evolve separately.

Please bear in mind that currently it's work-in-progress and the outcome cannot be verified in any way other than looking into sources (the develop branch represents current progress)

The first release should be merged with the master branch and available by the end of this year (2015).