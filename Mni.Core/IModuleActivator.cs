﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mni.Core;

namespace Mni.Core
{
    /// <summary>
    /// Customizes the starting and stopping of a module.
    /// </summary>
    /// <remarks>
    /// <para><see cref="IModuleActivator"/> is an interface that may be implemented when a module is started or stopped. 
    /// The Framework can create instances of a module's activator as required.</para>
    /// <para>The activator type is defined in the activator header which can occur at most once.
    /// <code>Activator: qualified-class-name</code></para>
    /// <para>If an instance's <see cref="IModuleActivator.Start(IModuleContext)"/> method executes successfully, it is guaranteed that 
    /// the same instance's <see cref="IModuleActivator.Stop(IModuleContext)"/> method will be called when the module is to be stopped.</para>
    /// <para>The Framework must not concurrently call an activator object.</para>
    /// <para>The specified activator class must have a public constructor that takes no parameters so that it can be created via reflection.</para>
    /// </remarks>
    public interface IModuleActivator
    {
        /// <summary>
        /// Called when this module is started so the Framework can perform the module-specific activities necessary to start this module. 
        /// </summary>
        /// <param name="context">The execution context of the module being started. </param>
        /// <remarks>
        /// This method can be used to register services or to allocate any resources that this module needs.
        /// This method must complete and return to its caller in a timely manner. 
        /// </remarks>
        void Start(IModuleContext context);

        /// <summary>
        /// Called when this module is stopped so the Framework can perform the module-specific activities necessary to stop the module. 
        /// </summary>
        /// <param name="context">The execution context of the module being stopped. </param>
        /// <remarks>
        /// <para>In general, this method should undo the work that the <see cref="IModuleActivator.Start(IModuleContext)"/> method started.</para>
        /// <para>There should be no active threads that were started by this module when this module returns.</para>
        /// <para>A stopped module must not call any Framework objects.</para>
        /// </remarks>
        void Stop(IModuleContext context);
    }
}
