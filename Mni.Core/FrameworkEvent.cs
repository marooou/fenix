﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public sealed class FrameworkEvent : EventArgs
    {
        public IModule Module { get; private set; }
        public FrameworkEventType Type { get; private set; }
        public Exception Exception { get; private set; }

        public FrameworkEvent(IModule module, FrameworkEventType type)
            : this(module, type, null)
        {
        }

        public FrameworkEvent(IModule module, FrameworkEventType type, Exception exception)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module");
            }

            this.Module = module;
            this.Type = type;
            this.Exception = exception;
        }
    }
}
