﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// An installed bundle in the Framework. 
    /// </summary>
    /// <remarks>
    /// A <see cref="IModule"/> object is the access point to define the lifecycle of an installed bundle. 
    /// Each bundle installed in the environment must have an associated <see cref="IModule"/> object. 
    /// </remarks>
    public interface IModule : IEquatable<IModule>
    {
        long Id { get; }
        IModuleManifest Manifest { get; }
        string Location { get; }
        IModuleContext Context { get; }

        /// <summary>
        /// Gets this bundle's current state. 
        /// </summary>
        /// <remarks>
        /// A bundle can be in only one state at any time. 
        /// </remarks>
        ModuleState State { get; }

        Assembly[] Assemblies { get; }

        /// <summary>
        /// Starts this bundle. 
        /// </summary>
        /// <exception cref="InvalidOperationException">If this bundle's state is <see cref="ModuleState.Uninstalled"/>.</exception>
        /// <remarks>
        /// The starting process looks as follows:
        /// <ol>
        /// <li>If this bundle is in the process of being activated or deactivated then this method must wait 
        /// for activation or deactivation to complete before continuing. If this does not occur in a reasonable time, 
        /// a <see cref="ModuleException"/> is thrown to indicate this bundle was unable to be started.</li>
        /// <li>If this bundle's state is <see cref="ModuleState.Active"/> then this method returns immediately. </li>
        /// <li>If this bundle's state is not <see cref="ModuleState.Resolved"/>, an attempt is made to resolve this bundle. If the Framework cannot resolve this bundle, a <see cref="ModuleException"/> is thrown. </li>
        /// <li>This bundle's state is set to <see cref="ModuleState.Starting"/>. </li>
        /// <li>A bundle event of type <see cref="ModuleEventType.Starting"/> is fired. </li>
        /// <li>
        /// The <see cref="IModuleActivator.Start"/> method of this bundle's <see cref="IModuleActivator"/>, if one is specified, is called.<br />
        /// If the <see cref="IModuleActivator"/> is invalid or throws an exception then: 
        /// <ul>
        ///  <li></li>
        /// </ul>
        /// </li>
        /// </ol>
        /// </remarks>
        void Start();
        void Stop();
        void Update(Stream stream);
        void Uninstall();

        IEnumerable<IServiceReference> GetRegisteredServices();
        IEnumerable<IServiceReference> GetServicesInUse();

        IEnumerable<string> FindEntries(String path, String filePattern, bool recurse);
    }
}