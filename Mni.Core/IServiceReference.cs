﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// A reference to a service.
    /// </summary>
    /// <remarks>
    /// <para>The Framework returns <see cref="IServiceReference"/> objects from the <see cref="IModuleContext.GetServiceReferences(Type)"/> method.</para>
    /// <para>A service reference object may be shared between bundles and can be used to examine the properties of the service and to get the service object.</para> 
    /// <para>Every service registered in the Framework has a unique <see cref="IServiceRegistration"/> object 
    /// and may have multiple, distinct <see cref="IServiceReference"/> objects referring to it.</para>
    /// <para><see cref="IServiceReference"/> objects associated with a <see cref="IServiceRegistration"/> object have the same hashCode 
    /// and are considered equal (more specifically, their <see cref="Object.Equals(Object)"/> method will return true when compared).</para>
    /// </remarks>
    public interface IServiceReference : IEquatable<IServiceReference>
    {
        /// <summary>
        /// Gets a collection of the keys in the properties dictionary of the service referenced by this <see cref="IServiceReference"/> object. 
        /// </summary>
        /// <remarks>
        /// <para>This method will continue to return the keys after the service has been unregistered. 
        /// This is so references to unregistered services (for example, ServiceReference objects stored in the log) 
        /// can still be interrogated.</para>
        /// <para>This method is case-preserving. This means that every key in the returned collection 
        /// must have the same case as the corresponding key in the properties dictionary that was passed 
        /// to the <see cref="IModuleContext.RegisterService"/> or <see cref="IServiceRegistration.SetProperties"/> methods.</para>
        /// </remarks>
        IEnumerable<string> PropertyKeys { get; }

        /// <summary>
        /// Gets the module that registered the service referenced by this <see cref="IServiceReference"/> object.
        /// </summary>
        /// <remarks>
        /// <para>This method must return null when the service has been unregistered. This can be used to determine 
        /// if the service has been unregistered.</para>
        /// </remarks>
        IModule Provider { get; }

        /// <summary>
        /// Returns the bundles that are using the service referenced by this <see cref="IServiceReference"/> object. 
        /// Specifically, this method returns the bundles whose usage count for that service is greater than zero. 
        /// </summary>
        /// <returns>An array of bundles whose usage count for the service referenced by this <see cref="IServiceReference"/> 
        /// object is greater than zero; an empty collection if no bundles are currently using that service.</returns>
        IEnumerable<IModule> GetUsingModules();

        /// <summary>
        /// Returns the property value to which the specified property key is mapped in the properties dictionary object of the service referenced by this <see cref="IServiceReference"/> object. 
        /// </summary>
        /// <param name="key">The property key. </param>
        /// <returns>The property value to which the key is mapped; null if there is no property named after the key.</returns>
        /// <remarks>
        /// <para>Property keys are case-insensitive. </para>
        /// <para>This method will continue to return property values after the service has been unregistered. 
        /// This is so references to unregistered services (for example, ServiceReference objects stored in the log) 
        /// can still be interrogated.</para>
        /// </remarks>
        object GetProperty(string key);
    }
}
