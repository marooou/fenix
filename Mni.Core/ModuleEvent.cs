﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    ///  An event from the Framework describing a module lifecycle change.
    /// </summary>
    /// <remarks>
    /// <para><see cref="ModuleEvent"/> objects are delivered to ModuleListeners (classes implementing <see cref="IModuleListener"/> interface) when a change occurs in a module's lifecycle.</para>
    /// <para>A type code is used to identify the event type for future extensibility. </para>
    /// </remarks>
    [Serializable]
    public sealed class ModuleEvent : EventArgs
    {
        /// <summary>
        /// Gets the module which had a lifecycle change. This module is the source of the event. 
        /// </summary>
        public IModule Target { get; private set; }

        /// <summary>
        /// Gets the module that was the origin of the event.
        /// </summary>
        /// <remarks>
        /// For the event type <see cref="ModuleState.Installed"/>, this is the module whose context was used to install the module. Otherwise it is the module itself. 
        /// </remarks>
        public IModule Origin { get; private set; }

        /// <summary>
        /// Gets the type of lifecycle event.
        /// </summary>
        public ModuleEventType Type { get; private set; }

        /// <summary>
        /// Creates a <see cref="ModuleEvent"/> of the specified type.
        /// </summary>
        /// <param name="target">
        /// The module which had a lifecycle change. This module is used as the origin of the event.
        /// </param>
        /// <param name="type">
        /// The event type.
        /// </param>
        public ModuleEvent(IModule target, ModuleEventType type)
            : this(target, type, null)
        {
        }

        /// <summary>
        /// Creates a <see cref="ModuleEvent"/> of the specified type.
        /// </summary>
        /// <param name="target">
        /// The module which had a lifecycle change. This module is used as the origin of the event.
        /// </param>
        /// <param name="type">
        /// The event type.
        /// </param>
        public ModuleEvent(IModule target, ModuleEventType type, IModule origin)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            this.Target = target;
            this.Type = type;
            this.Origin = origin ?? this.Target;
        }
    }
}
