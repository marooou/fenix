﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IFramework : IModule
    {
        void Init();
        void WaitForStop(int millis = -1);
    }
}
