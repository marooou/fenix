﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// An asynchronous <see cref="ModuleEvent"/> listener.
    /// </summary>
    /// <remarks>
    /// <para>When a <see cref="ModuleEvent"/> is fired, it is asynchronously delivered to a BundleListener. 
    /// The Framework delivers <see cref="ModuleEvent"/> objects to a <see cref="IModuleListener"/> in order and must not concurrently call a <see cref="IModuleListener"/>.
    /// <para>A BundleListener object is registered with the Framework using the <see cref="IModuleContext.AddModuleListener"/> method. 
    /// Listeners are called with a <see cref="ModuleEvent"/> object when a bundle has been installed, resolved, started, stopped, updated, unresolved, or uninstalled. </para></para>
    /// </remarks>
    public interface IModuleListener
    {
        /// <summary>
        /// Receives notification that a bundle has had a lifecycle change.
        /// </summary>
        /// <param name="moduleEvent">The <see cref="ModuleEvent"/> object</param>
        void ModuleChanged(ModuleEvent moduleEvent);
    }
}
