﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mni.Core;

namespace Mni.Core
{
    /// <summary>
    /// Denotes module identification capability.
    /// </summary>
    public interface IModuleAware
    {
        /// <summary>
        /// Gets the module associated with current object.
        /// </summary>
        IModule Module { get; }
    }
}
