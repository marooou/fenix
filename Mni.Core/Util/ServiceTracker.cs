﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core.Util
{
    public class ServiceTracker : IServiceTrackerCustomizer, IDisposable
    {
        private readonly IModuleContext _context;
        private readonly IServiceTrackerCustomizer _customizer;
        private readonly Func<IServiceReference, bool> _filter;

        private readonly object _listenerLock;
        private readonly object _referencesLock;

        private volatile IServiceListener _listener;
        private IList<IServiceReference> _references;

        public ServiceTracker(IModuleContext context)
            : this(context, r => true, null)
        {
        }

        public ServiceTracker(IModuleContext context, Type type)
            : this(context, r => r.Matches(type), null)
        {
        }

        public ServiceTracker(IModuleContext context, Type type, IServiceTrackerCustomizer customizer)
            : this(context, r => r.Matches(type), customizer)
        {
        }

        public ServiceTracker(IModuleContext context, IServiceReference reference, IServiceTrackerCustomizer customizer)
            : this(context, r => r.Equals(reference), customizer)
        {
        }

        public ServiceTracker(IModuleContext context, Func<IServiceReference, bool> filter, IServiceTrackerCustomizer customizer)
        {
            this._context = context;
            this._filter = filter;
            this._listenerLock = new object();
            this._referencesLock = new object();
            this._customizer = customizer ?? this;
        }

        public long? TrackingCount { get; private set; }

        public void Open()
        {
            bool added = false;
            if (this._listener == null)
            {
                lock (this._listenerLock)
                {
                    if (this._listener == null)
                    {
                        this._listener = new Listener(this);
                        added = true;
                    }
                }
            }

            if (added)
            {
                lock (this._referencesLock)
                {
                    this._references = new List<IServiceReference>();
                    this.TrackingCount = 0;
                }

                this._context.AddServiceListener(this._listener, evt => this._filter(evt.ServiceReference));
                lock (this._referencesLock)
                {
                    IEnumerable<IServiceReference> existingReferences = this._context.GetServiceReferences(this._filter);
                    foreach (var reference in existingReferences)
                    {
                        this._references.Add(reference);
                    }

                    // TODO: rewrite to https://github.com/apache/felix/blob/trunk/framework/src/main/java/org/osgi/util/tracker/BundleTracker.java
                }
            }
        }

        public void Close()
        {
            IServiceListener listenerToRemove = null;
            if (this._listener != null)
            {
                lock (this._listenerLock)
                {
                    if (this._listener != null)
                    {
                        listenerToRemove = this._listener;
                        this._listener = null;
                    }
                }
            }

            if (listenerToRemove != null) 
            {
                this._context.RemoveServiceListener(listenerToRemove);
                lock (this._referencesLock)
                {
                    this._references = null;
                    this.TrackingCount = null;
                }
            }
        }
        
        public virtual void AddingService(IServiceReference reference)
        {
        }

        public virtual void ModifiedService(IServiceReference reference)
        {
        }

        public virtual void RemovedService(IServiceReference reference)
        {
        }

        public void Dispose()
        {
            this.Close();
        }

        private void OnRegistered(IServiceReference reference)
        {
            lock (this._referencesLock)
            {
                this._references.Add(reference);
                this.TrackingCount++;
            }
        }

        private void OnModified(IServiceReference reference)
        {
            if (!this._filter(reference))
            {
                lock (this._referencesLock)
                {
                    this._references.Remove(reference);
                    this.TrackingCount++;
                }
            }
        }

        private void OnUnregistering(IServiceReference reference)
        {
            lock (this._referencesLock)
            {
                this._references.Remove(reference);
                this.TrackingCount++;
            }
        }

        private class Listener : IServiceListener
        {
            private readonly ServiceTracker _tracker;

            public Listener(ServiceTracker tracker)
            {
                this._tracker = tracker;
            }

            public void ServiceChanged(ServiceEvent serviceEvent)
            {
                switch (serviceEvent.Type)
                {
                    case ServiceEventType.Registered:
                        this._tracker.OnRegistered(serviceEvent.ServiceReference);
                        this._tracker._customizer.AddingService(serviceEvent.ServiceReference);
                        break;
                    case ServiceEventType.Modified:
                        this._tracker.OnModified(serviceEvent.ServiceReference);
                        this._tracker._customizer.ModifiedService(serviceEvent.ServiceReference);
                        break;
                    case ServiceEventType.Unregistering:
                        this._tracker.OnUnregistering(serviceEvent.ServiceReference);
                        this._tracker._customizer.RemovedService(serviceEvent.ServiceReference);
                        break;
                }
            }
        }
    }
}
