﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core.Util
{
    public static class IModuleContextExtensions
    {
        public static IModule GetFramework(this IModuleContext context)
        {
            return context.GetModule(0);
        }

        public static IEnumerable<IServiceReference> GetServiceReferences<S>(this IModuleContext context)
        {
            return context.GetServiceReferences(r => r.Matches(typeof(S)));
        }

        public static IEnumerable<IServiceReference> GetServiceReferences(this IModuleContext context, Type type)
        {
            return context.GetServiceReferences(r => r.Matches(type));
        }

        public static IServiceReference GetServiceReference(this IModuleContext context, Type type)
        {
            return context.GetServiceReferences(type).FirstOrDefault();
        }

        public static IServiceReference GetServiceReference<S>(this IModuleContext context)
        {
            return context.GetServiceReferences<S>().FirstOrDefault();
        }

        public static S GetService<S>(this IModuleContext context, IServiceReference reference)
        {
            return (S)context.GetService(reference);
        }
    }
}
