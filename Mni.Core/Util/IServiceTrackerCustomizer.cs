﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core.Util
{
    public interface IServiceTrackerCustomizer
    {
        void AddingService(IServiceReference reference);
        void ModifiedService(IServiceReference reference);
        void RemovedService(IServiceReference reference);
    }
}
