﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core.Util
{
    public static class IServiceReferenceExtensions
    {
        public static IEnumerable<IServiceReference> WithProperty(this IEnumerable<IServiceReference> references, string propertyName)
        {
            return references.Where(r => r.GetProperty(propertyName) != null);
        }

        public static IEnumerable<IServiceReference> WithProperty<TProperty>(this IEnumerable<IServiceReference> references, string propertyName)
            where TProperty : class
        {
            return references.Where(r => FilterSafe<TProperty>(r, propertyName, prop => true));
        }

        public static IEnumerable<IServiceReference> WithProperty<TProperty>(this IEnumerable<IServiceReference> references, string propertyName, Func<TProperty, bool> filter)
            where TProperty : class
        {
            return references.Where(r => FilterSafe(r, propertyName, filter));
        }

        public static T GetProperty<T>(this IServiceReference reference)
        {
            return (T)reference.GetProperty(typeof(T).FullName);
        }

        public static T GetProperty<T>(this IServiceReference reference, string name)
        {
            return (T)reference.GetProperty(name);
        }
        
        public static ComponentScope GetScope(this IServiceReference reference)
        {
            return reference.GetProperty<ComponentScope>(Constants.Component.Scope);
        }

        public static ComponentActivation GetActivation(this IServiceReference reference)
        {
            return reference.GetProperty<ComponentActivation>(Constants.Component.Activation);
        }

        public static string[] GetObjectClass(this IServiceReference reference)
        {
            return reference.GetProperty<string[]>(Constants.Component.ObjectClass);
        }

        public static bool Matches(this IServiceReference reference, Type type)
        {
            string[] objectClass = reference.GetObjectClass();
            return objectClass.Contains(type.FullName);
        }

        public static bool MatchesAny(this IServiceReference reference, params Type[] types)
        {
            string[] objectClass = reference.GetObjectClass();
            return types.Select(t => t.FullName).Any(n => objectClass.Contains(n));
        }

        public static bool MatchesAll(this IServiceReference reference, params Type[] types)
        {
            string[] objectClass = reference.GetObjectClass();
            return types.Select(t => t.FullName).All(n => objectClass.Contains(n));
        }

        private static bool FilterSafe<TProperty>(IServiceReference reference, String propertyName, Func<TProperty, bool> filter)
            where TProperty : class
        {
            try
            {
                var property = (TProperty)reference.GetProperty(propertyName);
                return property != null && filter(property);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
