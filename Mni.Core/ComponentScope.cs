﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public enum ComponentScope
    {
        Transient,
        Module,
        Singleton
    }
}
