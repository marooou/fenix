﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IModuleContext
    {
        IModule Module { get; }

        IModule GetModule(long id);
        IEnumerable<IModule> GetModules();

        IServiceRegistration RegisterService(Type instance, Type[] interfaces, IDictionary<string, object> properties);

        IEnumerable<IServiceReference> GetServiceReferences(Func<IServiceReference, bool> filter);

        object GetService(IServiceReference serviceReference);

        bool UngetService(IServiceReference serviceReference, object service);
        
        IModule InstallModule(string path);

        object Resolve(Type type);

        void AddServiceListener(IServiceListener listener, Func<ServiceEvent, bool> filter);
        void RemoveServiceListener(IServiceListener listener);

        void AddModuleListener(IModuleListener listener, Func<ModuleEvent, bool> filter);
        void RemoveModuleListener(IModuleListener listener);

        void AddFrameworkListener(IFrameworkListener listener);
        void RemoveFrameworkListener(IFrameworkListener listener);
    }
}
