﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IFrameworkListener
    {
        void FrameworkEvent(FrameworkEvent eventObject);
    }
}
