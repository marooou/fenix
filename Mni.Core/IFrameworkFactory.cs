﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IFrameworkFactory
    {
        IFramework NewFramework(IDictionary<string, string> configuration = null);
    }
}
