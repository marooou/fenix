﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    ///  A type code used to differentiate events describing modules lifecycle changes. 
    /// </summary>
    [Serializable]
    public enum ModuleEventType
    {
        /// <summary>
        /// The module has been installed.
        /// </summary>
        Installed = 1,

        /// <summary>
        /// The module has been resolved.
        /// </summary>
        Resolved = 2,

        /// <summary>
        /// The module has been started.
        /// </summary>
        /// <remarks>
        /// The module's <see cref="IModuleActivator.Start(IModuleContext)"/> has been 
        /// executed if the module has a module activator class. 
        /// </remarks>
        Started = 3,

        /// <summary>
        /// The module is about to be activated. 
        /// </summary>
        /// <remarks>
        /// The module's <see cref="IModuleActivator.Start(IModuleContext)"/> is about to be executed
        /// executed if the module has a module activator class. 
        /// </remarks>
        Starting = 4,

        /// <summary>
        /// The module has been stopped.
        /// </summary>
        /// <remarks>
        /// The module's <see cref="IModuleActivator.Stop(IModuleContext)"/> method has been 
        /// executed if the module has a module activator class. 
        /// </remarks>
        Stopped = 5,

        /// <summary>
        /// The module is about to be deactivated.
        /// </summary>
        /// <remarks>
        /// The module's <see cref="IModuleActivator.Stop(IModuleContext)"/> is about to be executed
        /// executed if the module has a module activator class. 
        /// </remarks>
        Stopping = 6,

        /// <summary>
        /// The module has been uninstalled.
        /// </summary>
        Uninstalled = 7,

        /// <summary>
        /// The module has been unresolved.
        /// </summary>
        Unresolved = 8,

        /// <summary>
        /// The module has been updated.
        /// </summary>
        Updated = 9
    }
}
