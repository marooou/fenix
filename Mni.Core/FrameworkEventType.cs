﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    [Serializable]
    public enum FrameworkEventType
    {
        Error = 1,
        Started = 2,
        Stopped = 3,
        StoppedUpdate = 4,
        StoppedTimeout = 5
    }
}
