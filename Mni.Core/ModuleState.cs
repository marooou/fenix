﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// A state code to define the lifecycle of a module.
    /// </summary>
    [Flags]
    public enum ModuleState
    {
        /// <summary>
        /// The module is uninstalled and may not be used.
        /// </summary>
        /// <remarks>
        /// The <see cref="Uninstalled"/> state is only visible after a module is uninstalled; 
        /// the module is in an unusable state but references to the module object may still be available and used for introspection. 
        /// </remarks>
        Uninstalled = 1,

        /// <summary>
        /// The module is installed but not yet resolved.
        /// </summary>
        /// <remarks>
        /// <para>A module is in the <see cref="Resolved"/> state when it has been installed in the Framework but is not or cannot be resolved.</para>
        /// <para>This state is visible if the module's code dependencies are not resolved. The Framework may attempt to resolve an <see cref="Installed"/> 
        /// module's code dependencies and move the module to the <see cref="Resolved"/> state.</para>
        /// </remarks>
        Installed = 2,

        /// <summary>
        /// The module is resolved and is able to be started.
        /// </summary>
        /// <remarks>
        /// <para>A module is in the <see cref="Resolved"/> state when the Framework has successfully resolved the module's code dependencies. </para>
        /// <para>Note that the module is not active yet. A module must be put in the RESOLVED state before it can be started. The Framework may attempt to resolve a module at any time. </para>
        /// </remarks>
        Resolved = 4,

        /// <summary>
        /// The module is in the process of starting.
        /// </summary>
        /// <remarks>
        /// <para>A module is in the <see cref="Starting"/> state when its <see cref="IModule.Start()"/> method is active. </para>
        /// <para>A module must be in this state when the module's <see cref="IModuleActivator.Start(IModuleContext)"/> is called. </para>
        /// <para>If the <see cref="IModuleActivator.Start(IModuleContext)"/> method completes without exception, then the module has successfully started and must move to the <see cref="Active"/> state. </para>
        /// </remarks>
        Starting = 8,

        /// <summary>
        /// The module is now running.
        /// </summary>
        /// <remarks>
        /// A module is in the ACTIVE state when it has been successfully started and activated. 
        /// </remarks>
        Active = 16,

        /// <summary>
        /// The module is in the process of stopping.
        /// </summary>
        /// <remarks>
        /// <para>A module is in the <see cref="Stopping"/> state when its <see cref="IModule.Stop()"/> method is active.</para>
        /// <para>A module must be in this state when the module's <see cref="IModuleActivator.Stop(IModuleContext)"/> method is called.</para>
        /// <para>When the <see cref="IModuleActivator.Stop(IModuleContext)"/> method completes the module is stopped and must move to the <see cref="Resolved"/> state.</para>
        /// </remarks>
        Stopping = 32
    }
}
