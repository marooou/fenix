﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// A synchronous <see cref="ModuleEvent"/> listener.
    /// </summary>
    /// <remarks>
    /// <para>When a <see cref="ModuleEvent"/> is fired, it is synchronous delivered to a BundleListener. 
    /// The Framework may deliver <see cref="ModuleEvent"/> objects to a <see cref="IModuleListener"/> out of order and may concurrently call and/or reenter a <see cref="IModuleListener"/>.
    /// <para>A BundleListener object is registered with the Framework using the <see cref="IModuleContext.AddModuleListener"/> method. 
    /// Listeners are called with a <see cref="ModuleEvent"/> object when a bundle has been installed, resolved, started, stopped, updated, unresolved, or uninstalled. </para></para>
    /// </remarks>
    public interface ISynchronousModuleListener : IModuleListener
    {
    }
}
