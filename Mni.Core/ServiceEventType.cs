﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// A type code used to differentiate events describing service lifecycle changes.
    /// </summary>
    [Serializable]
    public enum ServiceEventType
    {
        /// <summary>
        /// This service is in the process of being unregistered. 
        /// </summary>
        /// <remarks>
        /// This event is synchronously delivered <strong>after</strong> the service has been registered with the Framework. 
        /// </remarks>
        Registered = 1,

        /// <summary>
        /// The properties of a registered service have been modified and the new properties no longer match the listener's filter. 
        /// </summary>
        /// <remarks>
        /// This event is synchronously delivered <strong>after</strong> the service properties have been modified. 
        /// </remarks>
        Modified = 2,

        /// <summary>
        /// This service is in the process of being unregistered. 
        /// </summary>
        /// <remarks>
        /// <para>
        /// This event is synchronously delivered <strong>before</strong> the service has completed unregistering. 
        /// </para>
        /// <para>If a bundle is using a service that is <see cref="Unregistering"/>, 
        /// the bundle should release its use of the service when it receives this event. 
        /// If the bundle does not release its use of the service when it receives this event, 
        /// the Framework will automatically release the bundle's use of the service 
        /// while completing the service unregistering operation.</para>
        /// </remarks>
        Unregistering = 3
    }
}
