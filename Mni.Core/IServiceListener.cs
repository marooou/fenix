﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// A <see cref="ServiceEvent"/> listener.
    /// </summary>
    /// <remarks>
    /// <para>When a <see cref="ServiceEvent"/> is fired, it is synchronously delivered to a <see cref="IServiceListener"/>. 
    /// The Framework may deliver <see cref="ServiceEvent"/> objects to a listener out of order and may concurrently call and/or reenter a ServiceListener.</para>
    /// <para>
    /// A <see cref="IServiceListener"/> object is registered with the Framework using the <see cref="IModuleContext.AddServiceListener"/> method. 
    /// <see cref="IServiceListener"/> objects are called with a <see cref="ServiceEvent"/> object when a service is registered, modified, or is in the process of unregistering. 
    /// </para>
    /// </remarks>
    public interface IServiceListener
    {
        /// <summary>
        /// Receives notification that a service has had a lifecycle change. 
        /// </summary>
        /// <param name="serviceEvent">The <see cref="ServiceEvent"/> object.</param>
        void ServiceChanged(ServiceEvent serviceEvent);
    }
}
