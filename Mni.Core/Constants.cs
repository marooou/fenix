﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public static class Constants
    {
        public static class Module
        {
            public const string SymbolicName = "SymbolicName";

            public const string Version = "Version";

            public const string Activator = "Activator";

            public const string FrameworkGuid = "FrameworkGuid";

            public const string ComponentScan = "ComponentScan";
        }

        public static class Component
        {
            public const string ObjectClass = "objectClass";

            public const string Id = "component.id";

            public const string Kind = "component.kind";

            public const string Scope = "component.scope";

            public const string Activation = "component.activation";

            public const string AutoDispose = "component.autoDispose";
        }
    }
}
