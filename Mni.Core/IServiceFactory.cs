﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IServiceFactory
    {
        object GetService(IModule module, IServiceRegistration registration);
        void UngetService(IModule module, IServiceRegistration registration, object service);
        void Destroy(IModule module, IServiceRegistration registration);
    }
}
