﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// An event from the Framework describing a service lifecycle change. 
    /// </summary>
    /// <remarks>
    /// <see cref="ServiceEvent"/> objects are delivered to objects implementing <see cref="IServiceListener"/> interface 
    /// when a change occurs in this service's lifecycle.
    /// </remarks>
    [Serializable]
    public sealed class ServiceEvent : EventArgs
    {
        /// <summary>
        /// Gets a reference to the service that had a change occur in its lifecycle. 
        /// </summary>
        /// <remarks>
        /// This reference is the source of the event. 
        /// </remarks>
        public IServiceReference ServiceReference { get; private set; }

        /// <summary>
        /// Gets the type of event.
        /// </summary>
        public ServiceEventType Type { get; private set; }

        /// <summary>
        /// Creates a new service event object.
        /// </summary>
        /// <param name="serviceReference">
        /// A <see cref="IServiceReference"/> object to the service that had a lifecycle change.
        /// </param>
        /// <param name="type">
        /// The event type.
        /// </param>
        public ServiceEvent(IServiceReference serviceReference, ServiceEventType type)
        {
            this.ServiceReference = serviceReference;
            this.Type = type;
        }
    }
}
