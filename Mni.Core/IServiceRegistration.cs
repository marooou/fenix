﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    /// <summary>
    /// Represents a registered service.
    /// </summary>
    /// <remarks>
    /// <para>The Framework returns a <see cref="IServiceRegistration"/> object when a <see cref="IModuleContext.RegisterService"/> method invocation is successful.</para>
    /// <para>The <see cref="IServiceRegistration"/> object is for the private use of the registering bundle and should not be shared with other bundles.</para>
    /// <para>The <see cref="IServiceRegistration"/> object may be used to update the properties of the service or to unregister the service. </para>
    /// </remarks>
    public interface IServiceRegistration
    {
        /// <summary>
        /// Returns a <see cref="IServiceReference"/> object for a service being registered. 
        /// </summary>
        /// <returns>A <see cref="IServiceReference"/> object</returns>
        IServiceReference GetReference();

        /// <summary>
        /// Updates the properties associated with a service. 
        /// </summary>
        /// <param name="properties">
        /// The properties for this service. See <see cref="Constants.Component"/> for a list of standard service property keys. 
        /// </param>
        /// <exception cref="InvalidOperationException">If this <see cref="IServiceRegistration"/> object has already been unregistered.</exception>
        /// <exception cref="ArgumentException">If <paramref name="properties"/> contains case variants of the same key name.</exception>
        /// <remarks>
        /// The following keys are set by the Framework and cannot be modified by this method:
        /// <ul>
        ///  <li><see cref="Constants.Component.Activation"/></li>
        ///  <li><see cref="Constants.Component.Scope"/></li>
        ///  <li><see cref="Constants.Component.ObjectClass"/></li>
        /// </ul>
        /// The following steps are required to modify service properties:
        /// <ul>
        ///  <li>The service's properties are replaced with the provided properties.</li>
        ///  <li>A service event of type <see cref="ServiceEventType.Modified"/> is fired.</li>
        /// </ul>
        /// No changes should be made to <paramref name="properties"/> parameter object after calling this method. 
        /// To update the service's properties this method should be called again. 
        /// </remarks>
        void SetProperties(IDictionary<string, object> properties);

        /// <summary>
        /// Unregisters a service. Remove a <see cref="IServiceRegistration"/> object from the Framework service registry. 
        /// </summary>
        /// <remarks>
        /// <para>All <see cref="IServiceReference"/> objects associated with this <see cref="IServiceRegistration"/> object can no longer be used to interact with the service once unregistering is complete.</para>
        /// The following steps are required to unregister a service:
        /// <ol>
        /// <li>The service is removed from the Framework service registry so that it can no longer be obtained.</li>
        /// <li>A service event of type <see cref="ServiceEventType.Unregistering"/> is fired so that bundles using this service can release their use of the service. Once delivery of the service event is complete, the <see cref="IServiceReference"/> objects for the service may no longer be used to get a service object for the service.</li>
        /// <li>For each bundle whose use count for this service is greater than zero:<br />
        /// The bundle's use count for this service is set to zero.</li>
        /// </ol>
        /// </remarks>
        void Unregister();
    }
}
