﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    public interface IModuleManifest : IEquatable<IModuleManifest>
    {
        string SymbolicName { get; }
        Version Version { get; }
        string Activator { get; }

        string GetProperty(string key, string defaultValue = null);
        string[] GetPropertyKeys();
    }
}
