﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mni.Core
{
    using System.Runtime.Serialization;

    public class ModuleException : Exception
    {
        public ModuleException()
        {
        }

        public ModuleException(string message)
            : base(message)
        {
        }

        public ModuleException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ModuleException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
