﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Runtime.Serialization;

    internal class ComponentResolutionException : Exception
    {
        public ComponentResolutionException(string message)
            : base(message)
        {
        }

        public ComponentResolutionException()
        {
        }

        public ComponentResolutionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ComponentResolutionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
