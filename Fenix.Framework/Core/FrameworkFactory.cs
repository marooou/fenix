﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Mni.Core;

    public sealed class FrameworkFactory : IFrameworkFactory
    {
        private static readonly string[] ForbiddenPropertyKeys = new[]
            {
                Constants.Module.FrameworkGuid,
                Constants.Module.SymbolicName,
                Constants.Module.Version
            };

        public IFramework NewFramework(IDictionary<string, string> configuration = null)
        {
            if (configuration != null)
            {
                var forbiddenKey = configuration.Select(kv => kv.Key).FirstOrDefault(k => ForbiddenPropertyKeys.Contains(k));
                if (forbiddenKey != null)
                {
                    throw new ArgumentException(string.Format("Key '{0}' is reserved and will be provided by the framework", forbiddenKey));
                }
            }

            return new FrameworkModule(configuration);
        }
    }
}
