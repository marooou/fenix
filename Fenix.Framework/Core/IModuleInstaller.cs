﻿namespace Fenix.Framework.Core
{
    using Mni.Core;

    internal interface IModuleInstaller
    {
        IModule InstallModule(string path);
        void UninstallModule(IModule module);
    }
}
