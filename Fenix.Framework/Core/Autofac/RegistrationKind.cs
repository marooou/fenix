﻿namespace Fenix.Framework.Core
{
    using System;

    internal enum RegistrationKind : short
    {
        Any = 0,
        Dependency = 1,
        Service = 2,
    }
}
