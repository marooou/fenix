﻿namespace Fenix.Framework.Core
{
    using System.Collections.Generic;

    using Fenix.Framework.Core.ServiceLayer;

    using global::Autofac.Core;

    internal interface IModuleComponentRegistry : IComponentRegistry
    {
        IServiceRegistry ServiceRegistry { get; set; }

        IComponentRegistry LocalRegistry { get; }

        IEnumerable<IComponentRegistration> RegistrationsFor(RegistrationDetails details);

        bool TryGetRegistration(RegistrationDetails details, out IComponentRegistration registration);
    }
}
