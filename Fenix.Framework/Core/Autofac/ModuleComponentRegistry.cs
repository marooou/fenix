﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using global::Autofac.Core.Lifetime;

    using global::Autofac;
    using global::Autofac.Core;
    using global::Autofac.Core.Registration;
    using global::Autofac.Util;

    using Fenix.Framework.Core.ServiceLayer;

    using Mni.Core;

    using IModule = Mni.Core.IModule;

    internal class ModuleComponentRegistry : Disposable, IModuleComponentRegistry, IModuleAware
    {
        private static readonly Service StartableService = new TypedService(typeof(IStartable));

        public ModuleComponentRegistry(IModule module, IServiceRegistry serviceRegistry)
        {
            this.ServiceRegistry = serviceRegistry;
            this.LocalRegistry = new ComponentRegistry();
            this.Module = module;
        }

        public IServiceRegistry ServiceRegistry { get; set; }

        public IComponentRegistry LocalRegistry { get; private set; }

        public bool TryGetRegistration(Service service, out IComponentRegistration registration)
        {
            return this.TryGetRegistration(new RegistrationDetails(service), out registration);
        }

        public bool TryGetRegistration(RegistrationDetails details, out IComponentRegistration registration)
        {
            IComponentRegistration reg = this.RegistrationsFor(details).FirstOrDefault();
            registration = reg;
            return reg != null;
        }

        public bool IsRegistered(Service service)
        {
            return this.LocalRegistry.IsRegistered(service);
        }

        public void Register(IComponentRegistration registration)
        {
            this.LocalRegistry.Register(registration);
        }

        public void Register(IComponentRegistration registration, bool preserveDefaults)
        {
            this.LocalRegistry.Register(registration, preserveDefaults);
        }

        public IEnumerable<IComponentRegistration> RegistrationsFor(Service service)
        {
            return this.RegistrationsFor(new RegistrationDetails(service));
        }

        public IEnumerable<IComponentRegistration> RegistrationsFor(RegistrationDetails details)
        {
            var foundRegistrations = new List<IComponentRegistration>();
            bool wildcard = details.Kind == RegistrationKind.Any;
            if (this.ServiceRegistry != null && (details.Kind == RegistrationKind.Service || wildcard))
            {
                IModule searchModule = this.ShouldSearchGlobally(details) ? null : this.Module;

                IEnumerable<ServiceRegistration> registrations = this.ServiceRegistry.GetRegistrationsBy(searchModule).Cast<ServiceRegistration>();
                IEnumerable<IComponentRegistration> servicePart = registrations.Where(r => r.ComponentRegistration.Services.Any(s => s.Equals(details.Service)))
                                                                               .Select(r => r.ComponentRegistration);
                foundRegistrations.AddRange(servicePart);
            }

            if (details.Kind == RegistrationKind.Dependency || wildcard)
            {
                IEnumerable<IComponentRegistration> dependencyPart = this.LocalRegistry.RegistrationsFor(details.Service);
                foundRegistrations.AddRange(dependencyPart);
            }

            return foundRegistrations;
        }

        private bool IsStartable(Service service)
        {
            return service.Description == "AutoActivate" || service == StartableService;
        }

        public void AddRegistrationSource(IRegistrationSource source)
        {
            this.LocalRegistry.AddRegistrationSource(source);
        }

        public IEnumerable<IComponentRegistration> Registrations
        {
            get { return this.LocalRegistry.Registrations; }
        }

        public IEnumerable<IRegistrationSource> Sources
        {
            get { return this.LocalRegistry.Sources; }
        }

        public bool HasLocalComponents
        {
            get { return this.LocalRegistry.HasLocalComponents; }
        }

        public event EventHandler<ComponentRegisteredEventArgs> Registered
        {
            add { this.LocalRegistry.Registered += value; }
            remove { this.LocalRegistry.Registered -= value; }
        }

        public event EventHandler<RegistrationSourceAddedEventArgs> RegistrationSourceAdded
        {
            add { this.LocalRegistry.RegistrationSourceAdded += value; }
            remove { this.LocalRegistry.RegistrationSourceAdded -= value; }
        }

        public IModule Module { get; private set; }


        private bool ShouldSearchGlobally(RegistrationDetails details)
        {
            return details.GlobalLookup && !this.IsStartable(details.Service);
        }
    }
}
