﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Autofac.Core.Registration;

    using Fenix.Framework.Core.ServiceLayer;

    using Mni.Core;
    using Mni.Enterprise.Cdi;

    using global::Autofac;
    using global::Autofac.Builder;
    using global::Autofac.Core;

    internal static class RegistrationExtensions
    {
        public static bool MatchesKind(this IComponentRegistration componentRegistration, RegistrationKind kind)
        {
            if (kind == RegistrationKind.Any)
            {
                return true;
            }

            RegistrationKind registrationKind;
            if (componentRegistration.Metadata.TryGetValue(Constants.Component.Kind, out registrationKind))
            {
                return registrationKind == kind;
            }
            
            return false;
        }

        public static bool IsAutoDisposable(this IServiceRegistration registration)
        {
            IServiceReference reference = registration.GetReference();
            object autoDispose = reference.GetProperty(Constants.Component.AutoDispose);
            return (autoDispose == null || (bool)autoDispose);
        }
        
        public static bool MatchesType(this IComponentRegistration componentRegistration, Type type)
        {
            return componentRegistration.Services.OfType<IServiceWithType>()
                                                 .Any(typedService => typedService.ServiceType == type);
        }

        public static bool Update(this IDictionary<string, object> dictionary, IDictionary<string, object> updateDictionary, IEnumerable<string> keysToOmit = null)
        {
            if (updateDictionary == null) 
            {
                throw new ArgumentNullException("updateDictionary");
            }

            if (keysToOmit == null)
            {
                keysToOmit = Enumerable.Empty<string>();
            }

            bool changed = false;
            var toOmit = keysToOmit as string[] ?? keysToOmit.ToArray();
            foreach (var kv in updateDictionary)
            {
                if (toOmit.Contains(kv.Key))
                {
                    continue;
                }

                if (dictionary.ContainsKey(kv.Key))
                {
                    object value = dictionary[kv.Key];
                    if ((value != null && !value.Equals(kv.Value)) || (value == null && kv.Value != null))
                    {
                        dictionary[kv.Key] = kv.Value;
                    }
                }
                else
                {
                    dictionary.Add(kv);
                    changed = true;
                }
            }

            return changed;
        }

        public static bool TryGetValue<T>(this IDictionary<string, object> dict, string key, out T value)
        {
            return dict.TryGetValue(key, out value, default(T));
        }

        public static bool TryGetValue<T>(this IDictionary<string, object> dict, string key, out T value, T defaultValue)
        {
            object valueObj;
            if (dict.TryGetValue(key, out valueObj) && valueObj is T)
            {
                value = (T)valueObj;
                return true;
            }

            value = defaultValue;
            return false;
        }
        
        public static IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> As(
            this IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> registration,
            RegistrationKind kind,
            Type[] types)
        {
            // mark service type
            if (!registration.RegistrationData.Metadata.ContainsKey(Constants.Component.Kind))
            {
                registration.WithMetadata(Constants.Component.Kind, kind);
            }
            else
            {
                throw new InvalidOperationException("Registration kind was already provided");
            }

            // register all interfaces
            if (types == null || types.Length == 0)
            {
                registration.AsImplementedInterfaces();
                var serviceCount = registration.RegistrationData.Services.Count();
                if (serviceCount != 0)
                {
                    string[] objectClass = registration.RegistrationData.Services.OfType<IServiceWithType>()
                                .Select(s => s.ServiceType.FullName)
                                .ToArray();
                    registration.WithMetadata(Constants.Component.ObjectClass, objectClass);
                }
                else
                {
                    if (kind == RegistrationKind.Dependency)
                    {
                        registration.AsSelf();
                        string[] objectClass = { registration.ActivatorData.ImplementationType.FullName };
                        registration.WithMetadata(Constants.Component.ObjectClass, objectClass);
                    }
                    else
                    {
                        throw new ComponentResolutionException(
                            "Type {0} does not implement any interfaces hence cannot be registered as a service");
                    }
                }
            }
            else
            {
                registration.As(types);
            }

            return registration;
        }

        public static IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> WithSettings(
            this IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> registration, 
            IDictionary<string, object> properties,
            IEnumerable<string> keysToOmit = null)
        {
            registration.RegistrationData.Metadata.Update(properties, keysToOmit);
            return registration;
        }

        public static IComponentRegistration ManagedBy(
            this IComponentRegistration registration, IModuleContainer container)
        {
            IComponentRegistration alteredRegistration = registration;
            if (registration.MatchesKind(RegistrationKind.Service) && !(registration.Activator is FixedContextActivator))
            {
                IInstanceActivator activator = new FixedContextActivator(registration.Activator, container);
                alteredRegistration = new ComponentRegistration(
                    registration.Id,
                    activator,
                    registration.Lifetime,
                    InstanceSharing.None, 
                    InstanceOwnership.ExternallyOwned,
                    registration.Services,
                    registration.Metadata);
            }

            return alteredRegistration;
        }
    }
}
