﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Fenix.Framework.Core.ServiceLayer;
    using global::Autofac;
    using global::Autofac.Core;
    using global::Autofac.Core.Activators.Delegate;
    using global::Autofac.Core.Lifetime;
    using global::Autofac.Core.Registration;
    using global::Autofac.Core.Resolving;
    using global::Autofac.Util;
    using Mni.Core;
    using IModule = Mni.Core.IModule;

    [DebuggerDisplay("Tag = {Tag}, IsDisposed = {IsDisposed}")]
    internal class ModuleContainer : Disposable, IModuleContainer, IServiceProvider
    {
        private static readonly Guid SelfRegistrationId = Guid.NewGuid();
        private readonly ISharingLifetimeScope _rootLifetimeScope;

        public IModuleComponentRegistry ModuleComponentRegistry { get; private set; }

        public IModule Module { get; private set; }
        
        /// <summary>
        /// Create a new container.
        /// </summary>
        public ModuleContainer(IModule module, IServiceRegistry serviceRegistry)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module");
            }

            this.Module = module;

            this.ModuleComponentRegistry = new ModuleComponentRegistry(module, serviceRegistry);
            this.ModuleComponentRegistry.Register(new ComponentRegistration(
                SelfRegistrationId,
                new DelegateActivator(
                    typeof(ModuleContainer), 
                    (c, p) => this),
                new CurrentScopeLifetime(),
                InstanceSharing.Shared,
                InstanceOwnership.ExternallyOwned,
                new Service[] { new TypedService(typeof(IModuleContainer)) },
                new Dictionary<string, object>()));
            this._rootLifetimeScope = new LifetimeScope(this.ModuleComponentRegistry, module);
        }

        /// <summary>
        /// Begin a new sub-scope. Instances created via the sub-scope
        /// will be disposed along with it.
        /// </summary>
        /// <returns>A new lifetime scope.</returns>
        public ILifetimeScope BeginLifetimeScope()
        {
            return this._rootLifetimeScope.BeginLifetimeScope();
        }

        /// <summary>
        /// Begin a new sub-scope. Instances created via the sub-scope
        /// will be disposed along with it.
        /// </summary>
        /// <param name="tag">The tag applied to the <see cref="ILifetimeScope"/>.</param>
        /// <returns>A new lifetime scope.</returns>
        public ILifetimeScope BeginLifetimeScope(object tag)
        {
            return this._rootLifetimeScope.BeginLifetimeScope(tag);
        }

        /// <summary>
        /// Begin a new nested scope, with additional components available to it.
        /// Component instances created via the new scope
        /// will be disposed along with it.
        /// </summary>
        /// <param name="configurationAction">Action on a <see cref="ContainerBuilder"/>
        /// that adds component registrations visible only in the new scope.</param>
        /// <returns>A new lifetime scope.</returns>
        public ILifetimeScope BeginLifetimeScope(Action<ContainerBuilder> configurationAction)
        {
            return this._rootLifetimeScope.BeginLifetimeScope(configurationAction);
        }

        /// <summary>
        /// Begin a new nested scope, with additional components available to it.
        /// Component instances created via the new scope
        /// will be disposed along with it.
        /// </summary>
        /// <param name="tag">The tag applied to the <see cref="ILifetimeScope"/>.</param>
        /// <param name="configurationAction">Action on a <see cref="ContainerBuilder"/>
        /// that adds component registrations visible only in the new scope.</param>
        /// <returns>A new lifetime scope.</returns>
        public ILifetimeScope BeginLifetimeScope(object tag, Action<ContainerBuilder> configurationAction)
        {
            return this._rootLifetimeScope.BeginLifetimeScope(tag, configurationAction);
        }

        /// <summary>
        /// Gets the disposer associated with this container. Instances can be associated
        /// with it manually if required.
        /// </summary>
        public IDisposer Disposer
        {
            get { return this._rootLifetimeScope.Disposer; }
        }

        /// <summary>
        /// Gets tag applied to the lifetime scope.
        /// </summary>
        /// <remarks>The tag applied to this scope and the contexts generated when
        /// it resolves component dependencies.</remarks>
        public object Tag
        {
            get { return this._rootLifetimeScope.Tag; }
        }

        /// <summary>
        /// Fired when a new scope based on the current scope is beginning.
        /// </summary>
        public event EventHandler<LifetimeScopeBeginningEventArgs> ChildLifetimeScopeBeginning
        {
            add { this._rootLifetimeScope.ChildLifetimeScopeBeginning += value; }
            remove { this._rootLifetimeScope.ChildLifetimeScopeBeginning -= value; }
        }

        /// <summary>
        /// Fired when this scope is ending.
        /// </summary>
        public event EventHandler<LifetimeScopeEndingEventArgs> CurrentScopeEnding
        {
            add { this._rootLifetimeScope.CurrentScopeEnding += value; }
            remove { this._rootLifetimeScope.CurrentScopeEnding -= value; }
        }

        /// <summary>
        /// Fired when a resolve operation is beginning in this scope.
        /// </summary>
        public event EventHandler<ResolveOperationBeginningEventArgs> ResolveOperationBeginning
        {
            add { this._rootLifetimeScope.ResolveOperationBeginning += value; }
            remove { this._rootLifetimeScope.ResolveOperationBeginning -= value; }
        }

        /// <summary>
        /// Gets associates services with the components that provide them.
        /// </summary>
        public IComponentRegistry ComponentRegistry
        {
            get { return this.ModuleComponentRegistry; }
        }

        /// <summary>
        /// Resolve an instance of the provided registration within the context.
        /// </summary>
        /// <param name="registration">The registration.</param>
        /// <param name="parameters">Parameters for the instance.</param>
        /// <returns>
        /// The component instance.
        /// </returns>
        /// <exception cref="ComponentNotRegisteredException"/>
        /// <exception cref="DependencyResolutionException"/>
        public object ResolveComponent(IComponentRegistration registration, IEnumerable<Parameter> parameters)
        {
            if (registration.MatchesKind(RegistrationKind.Service))
            {
                this.ServiceRegistry.GetService(this.Module, registration);
            }


            return this._rootLifetimeScope.ResolveComponent(registration, parameters);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._rootLifetimeScope.Dispose();
                this.ModuleComponentRegistry.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType">An object that specifies the type of service object
        /// to get.</param>
        /// <returns>
        /// A service object of type <paramref name="serviceType"/>.-or- null if there is
        /// no service object of type <paramref name="serviceType"/>.
        /// </returns>
        public object GetService(Type serviceType)
        {
            return ((IServiceProvider)this._rootLifetimeScope).GetService(serviceType);
        }

        public object GetService(IModule module, IServiceRegistration registration)
        {
            return this._rootLifetimeScope.ResolveComponent(((ServiceRegistration)registration).ComponentRegistration, Enumerable.Empty<Parameter>());
        }

        public void UngetService(IModule module, IServiceRegistration registration, object service)
        {
            if (service != null && registration.IsAutoDisposable()) 
            {
                var disposable = service as IDisposable;
                if (disposable != null)
                {
                    try
                    {
                        disposable.Dispose();
                    }
                    catch (Exception ex)
                    {
                        var error = new FrameworkEvent(module, FrameworkEventType.Error, ex);
                        // TODO: send error via IFrameworkModule
                    }
                }
            }
        }

        public void Destroy(IModule module, IServiceRegistration registration)
        {
            // TODO: Do nothing on destroy?... maybe dispose something.
        }

        private IServiceRegistry ServiceRegistry
        {
            get
            {
                return this.ModuleComponentRegistry.ServiceRegistry;
            }
        }
    }
}
