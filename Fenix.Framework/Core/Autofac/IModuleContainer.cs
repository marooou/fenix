﻿namespace Fenix.Framework.Core
{
    using Mni.Core;
    
    using global::Autofac;
    using global::Autofac.Core;

    internal interface IModuleContainer : IContainer, IServiceFactory, IModuleAware
    {
        IModuleComponentRegistry ModuleComponentRegistry { get; }
    }
}
