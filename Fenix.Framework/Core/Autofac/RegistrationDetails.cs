namespace Fenix.Framework.Core
{
    using System;

    using global::Autofac.Core;

    internal class RegistrationDetails
    {
        public Service Service { get; private set; }
        public RegistrationKind Kind { get; private set; }
        public bool GlobalLookup { get; private set; }

        public RegistrationDetails(Service service)
            : this(service, RegistrationKind.Any, true)
        {
        }

        public RegistrationDetails(Service service, RegistrationKind filter, bool globalLookup)
        {
            if (service == null) throw new ArgumentNullException("service");

            this.Service = service;
            this.Kind = filter;
            this.GlobalLookup = globalLookup;
        }
    }
}
