﻿namespace Fenix.Framework.Core.ServiceLayer
{
    using System.Collections.Generic;
    
    using global::Autofac;
    using global::Autofac.Core;
    using global::Autofac.Core.Activators;

    /// <summary>
    /// The service activator.
    /// </summary>
    internal class FixedContextActivator : InstanceActivator, IInstanceActivator
    {
        private readonly IInstanceActivator _serviceActivator;
        private readonly IComponentContext _context;
        
        public FixedContextActivator(IInstanceActivator serviceActivator, IComponentContext context)
            : base(serviceActivator.LimitType)
        {
            this._serviceActivator = serviceActivator;
            this._context = context;
        }

        public object ActivateInstance(IComponentContext context, IEnumerable<Parameter> parameters)
        {
            return this._serviceActivator.ActivateInstance(this._context ?? context, parameters);
        }
    }
}
