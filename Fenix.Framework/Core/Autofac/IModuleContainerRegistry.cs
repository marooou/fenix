﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;

    internal interface IModuleContainerRegistry
    {
        IEnumerable<IModuleContainer> GetContainers(Func<IModuleContainer, bool> filter = null);
    }
}
