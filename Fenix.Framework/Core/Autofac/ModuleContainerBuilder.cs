﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Fenix.Framework.Core.ServiceLayer;

    using Mni.Core;
    
    using Fenix.Framework.Enterprise.Cdi;

    using global::Autofac;

    using IModule = Mni.Core.IModule;

    internal class ModuleContainerBuilder : ContainerBuilder, IModuleAware
    {
        private readonly IEnumerable<IStereotypeHandler> _stereotypeHandlers;
        private readonly IServiceRegistry _serviceRegistry;

        public ModuleContainerBuilder(Module module)
            : this(module, Enumerable.Empty<IStereotypeHandler>(), null)
        {
        }

        public ModuleContainerBuilder(Module module, IEnumerable<IStereotypeHandler> stereotypeHandlers, IServiceRegistry serviceRegistry)
        {
            this.Module = module;
            this._stereotypeHandlers = stereotypeHandlers;
            this._serviceRegistry = serviceRegistry;
            this.Register(ctx => new ModuleContext(module, ctx.Resolve<IModuleContainer>()))
                .As<IModuleContext>().AsSelf().SingleInstance().AutoActivate();
            this.DefaultSources = true;
        }
        
        public bool DefaultSources { get; set; }

        public IModuleContainer BuildModule(bool serviceLookupDuringBuild = true)
        {
            if (this.DefaultSources)
            {
                this.RegisterDefaultRegistrationSources();
            }

            bool handlesStereotypes = this._stereotypeHandlers.Any();
            IEnumerable<Type> types = null;
            if (handlesStereotypes)
            {
                // TODO: filter by ComponentScan module property.
                types = this.Module.Assemblies.SelectMany(a => a.GetTypes());
                foreach (var type in types)
                {
                    foreach (var handler in this._stereotypeHandlers.OfType<IPrebuildStereotypeHandler>())
                    {
                        handler.PreBuild(this, type);
                    }
                }
            }

            var container = new ModuleContainer(this.Module, this._serviceRegistry);
            if (serviceLookupDuringBuild)
            {
                this.Update(container);
            }
            else
            {
                this.Update(container);
                container.ModuleComponentRegistry.ServiceRegistry = container.Resolve<IServiceRegistry>();
            }

            if (handlesStereotypes)
            {
                foreach (var type in types)
                {
                    foreach (var handler in this._stereotypeHandlers.OfType<IPostbuildStereotypeHandler>())
                    {
                        handler.PostBuild(container, type);
                    }
                }
            }

            return container;
        }

        public IModule Module { get; private set; }

        private void RegisterDefaultRegistrationSources()
        {
            var builder = new ContainerBuilder();

            // This will pretty much steal sources from default an autofac container
            var container = builder.Build();
            foreach (var source in container.ComponentRegistry.Sources) 
            {
                this.RegisterSource(source);
            }

            container.Dispose();
        }
    }
}
