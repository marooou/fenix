﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mni.Core;

namespace Fenix.Framework.Core
{
    internal interface IListenerRegistry<TListener>
    {
        void AddListener(IModule module, TListener listener, Func<EventArgs, bool> filter);

        void RemoveListener(IModule module, TListener listener);

        bool RemoveAllListeners(IModule module);

        IEnumerable<ListenerInfo<TListener>> GetListeners();
    }
}
