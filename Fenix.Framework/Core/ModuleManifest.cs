﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Mni.Core;

    internal class ModuleManifest : IModuleManifest
    {
        private readonly IDictionary<string, string> _properties;

        public ModuleManifest(IDictionary<string, string> properties)
        {
            if (properties == null) throw new ArgumentNullException("properties");
            this._properties = properties;
            this.Populate();
        }

        private void Populate()
        {
            string symbName;
            if (!this._properties.TryGetValue(Constants.Module.SymbolicName, out symbName) ||
                string.IsNullOrEmpty(symbName))
            {
                throw new ArgumentException(string.Format("Invalid {0} provided ({1})", Constants.Module.SymbolicName, symbName));
            }

            this.SymbolicName = symbName;

            string activator;
            this._properties.TryGetValue(Constants.Module.Activator, out activator);
            this.Activator = activator;

            string version;
            this._properties.TryGetValue(Constants.Module.Version, out version);
            try
            {
                this.Version = Version.Parse(version);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Invalid {0} provided ({1})", Constants.Module.Version, version), ex);
            }
        }

        public string SymbolicName { get; private set; }

        public Version Version { get; private set; }

        public string Activator { get; set; }

        public string GetProperty(string key, string defaultValue = null)
        {
            string value;
            if (this._properties.TryGetValue(key, out value))
            {
                return value;
            }

            return defaultValue;
        }

        public string[] GetPropertyKeys()
        {
            return this._properties.Keys.ToArray();
        }

        public bool Equals(IModuleManifest other)
        {
            if (other == null)
            {
                return false;
            }

            return this.SymbolicName == other.SymbolicName &&
                   this.Version.Equals(other.Version);
        }

        public override bool Equals(object other)
        {
            return this.Equals(other as IModuleManifest);
        }

        public override int GetHashCode()
        {
            return this.SymbolicName.GetHashCode() * 69 + 
                this.Version.GetHashCode();
        }
    }
}
