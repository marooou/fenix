﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using YamlDotNet.Serialization;

    using Mni.Core;

    internal class ModuleManifestReader : IModuleManifestReader
    {
        private const string ManifestFileName = "module.mf";

        public IModuleManifest ReadManifest(string moduleDir)
        {
            string manifestPath = Path.Combine(moduleDir, ManifestFileName);
            if (!File.Exists(manifestPath))
            {
                throw new FileNotFoundException(string.Format("Unable to find {0} manifest file in {1}", ManifestFileName, moduleDir));
            }

            IDictionary<string, string> properties = null;
            using (var sr = new StreamReader(manifestPath))
            {
                var deserializer = new Deserializer();
                var dictionary = deserializer.Deserialize(sr) as IDictionary<object, object>;

                properties = dictionary.ToDictionary(item => Convert.ToString(item.Key), item => Convert.ToString(item.Value));
            }

            return new ModuleManifest(properties);
        }

    }
}
