﻿namespace Fenix.Framework.Core
{
    using System.IO;

    using Mni.Core;

    internal class ModuleInstaller : IModuleInstaller
    {
        private readonly IModuleManifestReader _manifestReader;
        private readonly IFrameworkModule _frameworkContext;
        private long _nextId = 1;

        public ModuleInstaller(IModuleManifestReader manifestReader, IFrameworkModule frameworkContext)
        {
            this._manifestReader = manifestReader;
            this._frameworkContext = frameworkContext;
        }

        public IModule InstallModule(string path)
        {
            if (!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                path += Path.DirectorySeparatorChar;
            }

            var manifest = this._manifestReader.ReadManifest(path);
            var module = new Module(this._nextId, manifest, path, this._frameworkContext);
            this._nextId++;
            return module;
        }

        public void UninstallModule(IModule module)
        {
            // TODO: Any persistent storage area provided for given module by the Framework is removed.
        }
    }
}
