﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Globalization;
    using System.IO;
    using System.Reflection;

    using Mni.Core;

    internal class Module : IModule
    {
        private ModuleContext _context;

        public Module(long moduleId, IModuleManifest manifest, string location, IFrameworkModule frameworkContext)
        {
            if (manifest == null) throw new ArgumentNullException("manifest");
            if (location == null) throw new ArgumentNullException("location");
            if (frameworkContext == null) throw new ArgumentNullException("frameworkContext");

            if (!location.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
            {
                throw new ArgumentException("The module location must end with System.IO.Path.DirectorySeparatorChar");
            }

            this.Id = moduleId;
            this.Manifest = manifest;
            this.Location = location;
            this.Framework = frameworkContext;
            this.State = ModuleState.Installed;
        }
        
        protected Module(string location)
        {
            this.Location = location;
            this.State = ModuleState.Installed;
        }

        public long Id { get; protected set; }
        public IModuleManifest Manifest { get; protected set; }
        public string Location { get; protected set; }
        public ModuleState State { get; protected set; }
        public IModuleContext Context
        {
            get
            {
                return this._context;
            }
        }

        public Assembly[] Assemblies { get; protected set; }
        internal IFrameworkModule Framework { get; private set; }

        protected void InjectFramework(IFrameworkModule framework)
        {
            this.Framework = framework;
        }

        internal void BindContext(ModuleContext context)
        {
            this._context = context;
        }

        internal void UnbindContext()
        {
            this._context = null;
        }
        
        protected virtual void Resolve()
        {
            try
            {
                this.Assemblies = this.Framework.ModuleResolver.Resolve(this);
                this.ChangeState(ModuleState.Resolved);
                this.Notify(ModuleEventType.Resolved);
            }
            catch (Exception ex)
            {
                throw new ModuleException(string.Format("Unable to resolve {0} module", this), ex);
            }
        }

        public virtual void Start()
        {
            switch (this.State)
            {
                case ModuleState.Uninstalled:
                    throw new InvalidOperationException("Cannot start module which is uninstalled.");
                case ModuleState.Starting:
                case ModuleState.Stopping:
                    throw new ModuleException(string.Format("Cannot start module which is {0}.", this.State.ToString().ToLower()));
                case ModuleState.Active:
                    return;
                case ModuleState.Installed:
                    this.Resolve();
                    break;
            }

            try
            {
                this.Framework.ModuleRegistry.Start(this);

                this.ChangeState(ModuleState.Starting);
                this.Notify(ModuleEventType.Starting);
                this.Framework.ModuleRegistry.TryStartActivator(this);
                if (this.State == ModuleState.Uninstalled)
                {
                    throw new ModuleException(string.Format("The {0} module was uninstalled while starting", this));
                }

                this.ChangeState(ModuleState.Active);
                this.Notify(ModuleEventType.Started);
            }
            catch (Exception ex)
            {
                this.Stop(false);
                throw new ModuleException(string.Format("The {0} module could not be started", this), ex);
            }
        }

        public virtual void Stop()
        {
            switch (this.State)
            {
                case ModuleState.Uninstalled:
                    throw new InvalidOperationException("Cannot stop module which is uninstalled.");
                case ModuleState.Starting:
                case ModuleState.Stopping:
                    throw new ModuleException(string.Format("Cannot stop module which is {0}.", this.State.ToString().ToLower()));
                case ModuleState.Resolved:
                case ModuleState.Installed:
                    return;
            }

            this.Stop(true);
        }

        public virtual void Update(Stream stream)
        {
            throw new NotImplementedException("Updates have not yet been implemented");
        }

        private void Stop(bool stopActivator)
        {
            this.ChangeState(ModuleState.Stopping);
            this.Notify(ModuleEventType.Stopping);

            if (stopActivator)
            {
                this.Framework.ModuleRegistry.TryStopActivator(this);
            }

            this.ReleaseRegisteredServices();
            this.ReleaseUsedServices();

            this.Framework.ServiceRegistry.RemoveAllListeners(this);
            this.Framework.ModuleRegistry.RemoveAllListeners(this);

            this.Framework.ModuleRegistry.Stop(this);
            this.ChangeState(ModuleState.Resolved);
            this.Notify(ModuleEventType.Stopped);
            this.UnbindContext();
        }

        public virtual void Uninstall()
        {
            if (this.IsInState(ModuleState.Uninstalled))
            {
                throw new InvalidOperationException("Cannot uninstall module which is already uninstalled.");
            }

            try
            {
                if (this.IsInState(ModuleState.Starting | ModuleState.Stopping | ModuleState.Active))
                {
                    this.Stop();
                }
            }
            catch (Exception ex)
            {
                var errorEvent = new FrameworkEvent(this, FrameworkEventType.Error, ex);
                this.Framework.RaiseEvent(errorEvent);
            }

            if (!this.Framework.ModuleResolver.IsInUse(this))
            {
                this.Framework.ModuleRegistry.Unregister(this);
            }

            this.ChangeState(ModuleState.Uninstalled);
            this.Notify(ModuleEventType.Uninstalled);

            this.Framework.ModuleInstaller.UninstallModule(this);
        }

        public virtual IEnumerable<IServiceReference> GetRegisteredServices()
        {
            return this.Framework.ServiceRegistry.GetServicesRegisteredBy(this);
        }

        public virtual IEnumerable<IServiceReference> GetServicesInUse()
        {
            return this.Framework.ServiceRegistry.GetServicesUsedBy(this);
        }

        public IEnumerable<string> FindEntries(string path, string filePattern, bool recurse)
        {
            // TODO: make sure path is relative and not above Locations
            var absolutePath = Path.Combine(this.Location, path);
            int locationLen = this.Location.Length;
            return Directory.GetFiles(absolutePath, filePattern, recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                    .Select(x => x.Substring(locationLen));
        }

        public override string ToString()
        {
            return string.Format("[ Id: {0}, SymbolicName: {1}, Version : {2} ]", this.Id, this.Manifest.SymbolicName, this.Manifest.Version);
        }

        public override int GetHashCode()
        {
            return (int)(this.Id % int.MaxValue);
        }

        public override bool Equals(object other)
        {
            return this.Equals(other as IModule);
        }

        public bool Equals(IModule other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Id == other.Id;
        }

        protected void ReleaseRegisteredServices()
        {
            var serviceRegistrations = this.Framework.ServiceRegistry.GetRegistrationsBy(this);
            foreach (var serviceRegistration in serviceRegistrations)
            {
                serviceRegistration.Unregister();
            }
        }

        protected void ReleaseUsedServices()
        {
            this.Framework.ServiceRegistry.ReleaseServicesUsedBy(this);
        }

        protected void ChangeState(ModuleState type)
        {
            this.State = type;
        }

        protected bool IsInState(ModuleState filter)
        {
            return filter.HasFlag(this.State);
        }
        
        protected void Notify(ModuleEventType eventType)
        {
            var eventObject = new ModuleEvent(this, eventType);
            this.Framework.ModuleRegistry.RaiseEvent(eventObject);
        }
    }
}
