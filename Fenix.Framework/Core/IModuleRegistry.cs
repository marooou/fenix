﻿namespace Fenix.Framework.Core
{
    using System.Collections.Generic;

    using Mni.Core;

    using Fenix.Framework;
    using System;

    internal interface IModuleRegistry : IListenerRegistry<IModuleListener>
    {
        IModule GetModule(long moduleId);

        IEnumerable<IModule> GetModules();

        void Register(IModule module);

        void Start(Module module);

        bool TryStartActivator(IModule module);

        void Stop(Module module);

        bool TryStopActivator(IModule module);
        
        bool Unregister(IModule module);
        
        void RaiseEvent(ModuleEvent eventObject);

        /*
        bool AcquireGlobalLock();

        void ReleaseGlobalLock();

        void AcquireModuleLock(IModule module, ModuleState states);

        void ReleaseModuleLock(IModule module);
        */
    }
}
