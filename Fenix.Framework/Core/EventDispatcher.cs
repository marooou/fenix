﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Collections.Concurrent;
    using System.Threading;

    internal class EventDispatcher : IEventDispatcher
    {
        private readonly int _queueLimit;
        private readonly ManualResetEvent _stoppedEvent;

        private ConcurrentQueue<Action> _actionQueue;
        private Semaphore _actionNotification;

        private readonly object _threadLock;
        private Thread _thread;
        private bool _stopping;

        public EventDispatcher()
        {
            this._actionQueue = new ConcurrentQueue<Action>();
            this._stoppedEvent = new ManualResetEvent(false);
            this._threadLock = new object();
            this._queueLimit = int.MaxValue;
        }

        public void StartDispatching()
        {
            lock (this._threadLock)
            {
                if (this._stopping)
                {
                    throw new InvalidOperationException("Cannot start dispatcher which which was requested to stop.");
                }

                if ((this._thread == null || !this._thread.IsAlive))
                {
                    this._actionNotification = new Semaphore(0, this._queueLimit);
                    this._stoppedEvent.Reset();
                    this._thread = new Thread(this.Run);
                    this._thread.Start();
                }
            }
        }

        public void Dispatch<TListener, TEvent>(IEnumerable<TListener> listeners, TEvent eventObject, Action<TListener, TEvent> caller)
        {
            if (listeners == null) throw new ArgumentNullException("listeners");
            if (caller == null) throw new ArgumentNullException("caller");

            this.NotifyListeners(listeners, eventObject, caller);
        }

        public void DispatchAsync<TListener, TEvent>(IEnumerable<TListener> listeners, TEvent eventObject, Action<TListener, TEvent> caller)
        {
            if (listeners == null) throw new ArgumentNullException("listeners");
            if (caller == null) throw new ArgumentNullException("caller");

            // there's no point in enqueueing an idle action. 
            if (!listeners.Any())
            {
                return;
            }

            Action action = () => this.NotifyListeners(listeners, eventObject, caller);
            this.Dispatch(action);
        }

        public void StopDispatching(int millis = -1)
        {
            lock (this._threadLock)
            {
                if (this._thread == null)
                {
                    return;
                }

                if (!this._stopping)
                {
                    this._stopping = true;
                    this._actionNotification.Release();
                }
            }

            this._stoppedEvent.WaitOne(millis);

            lock (this._threadLock)
            {
                if (this._thread != null)
                {
                    this._thread.Abort();
                }
            }
        }

        private void Dispatch(Action action)
        {
            lock (this._threadLock)
            {
                if (this._thread == null || this._stopping)
                {
                    throw new InvalidOperationException("The dispatcher is either not started or stopping was requested");
                }

                this._actionQueue.Enqueue(action);
                try
                {
                    this._actionNotification.Release();
                }
                catch (SemaphoreFullException ex)
                {
                    throw new OverflowException("Dispatcher queue full.", ex);
                }
            }

        }

        private void Run()
        {
            try
            {
                while (true)
                {
                    this._actionNotification.WaitOne();
                    Action nextAction;
                    if (!this._actionQueue.TryDequeue(out nextAction))
                    {
                        lock (this._threadLock)
                        {
                            if (this._stopping) return;
                        }
                    }

                    nextAction();
                }
            }
            finally
            {
                lock (this._threadLock)
                {
                    try
                    {
                        this._actionNotification.Dispose();
                    }
                    finally
                    {
                        this._actionNotification = null;
                        this._thread = null;
                        this._stopping = false;

                        // just making sure there are no elements left in the queue in case the thread was aborted.
                        this._actionQueue = new ConcurrentQueue<Action>();
                        this._stoppedEvent.Set();
                    }
                }
            }
        }

        private void NotifyListeners<TListener, TEvent>(IEnumerable<TListener> listeners, TEvent eventObject, Action<TListener, TEvent> caller)
        {
            foreach (var listener in listeners)
            {
                caller(listener, eventObject);
            }
        }
    }
}
