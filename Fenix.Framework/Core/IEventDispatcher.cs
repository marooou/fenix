﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Exposes event dispatching capabilities
    /// </summary>
    /// <remarks>
    /// <para>When implemented, the dispatcher should have at most one thread active at all times.</para>
    /// <para>All public methods of a type implementing this interface should be thread-safe.</para>
    /// </remarks>
    internal interface IEventDispatcher
    {
        /// <summary>
        /// Starts the dispatching thread.
        /// </summary>
        /// <remarks>
        /// <para>This method makes it possible to use <see cref="IEventDispatcher.Dispatch"/> method.</para>
        /// <para>If the dispatcher thread was already started and is alive then this method returns immediately.</para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">If the dispatcher was requested to stop prior to this method call
        /// and has not yet been stopped.</exception>
        void StartDispatching();

        /// <summary>
        /// Immediately calls <paramref name="caller"/> action for each provided listener.
        /// </summary>
        /// <typeparam name="TListener">Type of listeners</typeparam>
        /// <typeparam name="TEvent">Type of event object</typeparam>
        /// <param name="listeners">A collection of listeners to be notified</param>
        /// <param name="eventObject">An event object to be passed to each listener</param>
        /// <param name="caller">An action involving a listener and an event object which eventually will be invoked for each listener</param>
        /// <exception cref="ArgumentNullException">If either <paramref name="listeners"/> or <paramref name="caller"/> is null/</exception>
        /// <remarks>
        ///  When a <paramref name="eventObject"/> is fired, it is synchronously delivered to all <paramref name="listeners"/>. 
        ///  The Framework may deliver event objects to listeners out of order and may concurrently call and/or reenter a listener.
        /// </remarks>
        void Dispatch<TListener, TEvent>(IEnumerable<TListener> listeners, TEvent eventObject, Action<TListener, TEvent> caller);

        /// <summary>
        /// Enqueues a dispatcher action of calling <paramref name="caller"/> action for each provided listener.
        /// </summary>
        /// <typeparam name="TListener">Type of listeners</typeparam>
        /// <typeparam name="TEvent">Type of event object</typeparam>
        /// <param name="listeners">A collection of listeners to be notified</param>
        /// <param name="eventObject">An event object to be passed to each listener</param>
        /// <param name="caller">An action involving a listener and an event object which eventually will be invoked for each listener</param>
        /// <exception cref="ArgumentNullException">If either <paramref name="listeners"/> or <paramref name="caller"/> is null/</exception>
        /// <exception cref="OverflowException">If the dispatcher's action queue is full and cannot accept more actions.</exception>        
        /// <remarks>
        ///  When a <paramref name="eventObject"/> is fired, it is asynchronously delivered to all <paramref name="listeners"/>. 
        ///  The Framework delivers event objects to listeners in order and must not concurrently call a listener.
        /// </remarks>
        void DispatchAsync<TListener, TEvent>(IEnumerable<TListener> listeners, TEvent eventObject, Action<TListener, TEvent> caller);

        /// <summary>
        /// Stops the dispatching thread.
        /// </summary>
        /// <param name="millis">A number of milliseconds after which the dispatching thread should be aborted if it is still active.</param>
        /// <remarks>
        /// <para>This method disables ability to add further actions using <see cref="IEventDispatcher.Dispatch"/> method
        /// and blocks until all previously dispatched events have been delivered.</para>
        /// <para>The dispatcher can wait for an indefinite amount of time, unless the <paramref name="millis"/> param is greater than zero
        /// in which case, after given period of time, the dispatching thread is aborted and the action queue is cleared.</para>
        /// <para></para>
        /// </remarks>
        void StopDispatching(int millis = -1);
    }
}
