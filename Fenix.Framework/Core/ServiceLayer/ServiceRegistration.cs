﻿namespace Fenix.Framework.Core.ServiceLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Fenix.Framework.Core;

    using Mni.Core;

    using global::Autofac.Core;
    using global::Autofac.Core.Registration;

    using IModule = Mni.Core.IModule;

    internal class ServiceRegistration : IServiceRegistration, IModuleAware
    {
        private static readonly string[] ReservedPropertyKeys = { Constants.Component.ObjectClass, Constants.Component.Activation, Constants.Component.Scope, Constants.Component.AutoDispose };
        private readonly IServiceRegistry _registry;
        private readonly ServiceReference _reference;
        private readonly object _stateLock;
        private readonly IServiceFactory _factory;

        private bool _valid;
        private volatile IDictionary<string,object> _properties;
        
        public ServiceRegistration(IModuleContainer owner, IComponentRegistration registration, IServiceRegistry registry)
            : this(owner, registration, registry, null)
        {
        }
        
        public ServiceRegistration(IModuleContainer owner, IComponentRegistration registration, IServiceRegistry registry, IServiceFactory factory)
        {
            this.Module = owner.Module;
            this.ComponentRegistration = registration;

            this._stateLock = new object();
            this._reference = new ServiceReference(this);
            this._registry = registry;
            this._valid = true;

            ComponentScope scope;
            this.ComponentRegistration.Metadata.TryGetValue(Constants.Component.Scope, out scope, ComponentScope.Singleton);
            this.Scope = scope;

            ComponentActivation activation;
            this.ComponentRegistration.Metadata.TryGetValue(Constants.Component.Activation, out activation, ComponentActivation.Eager);
            this.Activation = activation;

            this._properties = this.ComponentRegistration.Metadata;

            this._factory = factory ?? this.CreateServiceFactory(owner);
        }
        
        public IServiceReference GetReference()
        {
            this.CheckState();
            return this._reference;
        }
        
        public void SetProperties(IDictionary<string, object> properties)
        {
            if (properties == null)
            {
                return;
            }

            bool updated = false;
            lock (this._stateLock)
            {
                if (!this._valid)
                {
                    throw new InvalidOperationException("The registration was already unregistered.");
                }

                updated = this.ComponentRegistration.Metadata.Update(properties, ReservedPropertyKeys);
                this._properties = this.ComponentRegistration.Metadata;
            }

            if (updated)
            {
                var eventObject = new ServiceEvent(this._reference, ServiceEventType.Modified);
                this._registry.RaiseEvent(eventObject);
            }
        }

        public void Unregister()
        {
            this.CheckState();
            this._registry.DestroyRegistration(this);
        }

        public override bool Equals(object obj)
        {
            var serviceReg = obj as ServiceRegistration;
            if (serviceReg != null)
            {
                return this.Id == serviceReg.Id;
            }

            var serviceRef = obj as ServiceReference;
            if (serviceRef != null)
            {
                return this.Id == serviceRef.Registration.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.ComponentRegistration.Id.GetHashCode();
        }

        public IModule Module { get; private set; }

        internal IComponentRegistration ComponentRegistration { get; private set; }

        internal Guid Id
        {
            get { return this.ComponentRegistration.Id; }
        }

        internal ComponentScope Scope { get; private set; }
        internal ComponentActivation Activation { get; private set; }

        internal void Invalidate()
        {
            lock (this._stateLock)
            {
                this._factory.UngetService(this.Module, this, null);
                this._valid = false;
            } 
        }

        internal object AcquireService(IModule module)
        {
            return this._factory.GetService(module, this);
        }

        internal void ReleaseService(IModule module, object service)
        {
            this._factory.UngetService(module, this, service);
        }

        internal bool Valid
        {
            get
            {
                lock (this._stateLock)
                {
                    return this._valid;
                }
            }
        }

        internal IEnumerable<string> PropertyKeys
        {
            get 
            {
                return this._properties.Keys;
            }
        }

        internal IEnumerable<IModule> GetUsingModules()
        {
            return this._registry.GetUsingModules(this);
        }

        internal object GetProperty(string key)
        {
            object value = null;
            if (this._properties.TryGetValue(key, out value))
            {
                return value;
            }

            return null;
        }
        
        private void CheckState()
        {
            lock (this._stateLock)
            {
                if (!this._valid)
                {
                    throw new InvalidOperationException("The registration was already unregistered.");
                }
            }
        }

        private IServiceFactory CreateServiceFactory(IModuleContainer scope)
        {
            return this.Scope == ComponentScope.Singleton 
                ? new SingletonServiceFactory(scope) 
                : (IServiceFactory)scope;
        }
    }
}
