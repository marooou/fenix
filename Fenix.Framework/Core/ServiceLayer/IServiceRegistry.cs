﻿namespace Fenix.Framework.Core.ServiceLayer
{
    using System;
    using System.Collections.Generic;

    using Autofac.Core;

    using Mni.Core;

    using IModule = Mni.Core.IModule;

    internal interface IServiceRegistry : IListenerRegistry<IServiceListener>
    {
        IServiceRegistration CreateRegistration(IModuleContainer container, Type impl, Type[] ifs, IDictionary<string, object> properties);

        void DestroyRegistration(IServiceRegistration registration);

        IEnumerable<IServiceRegistration> GetRegistrations();

        IEnumerable<IServiceRegistration> GetRegistrationsBy(IModule module);

        IEnumerable<IModule> GetUsingModules(IServiceRegistration registration);

        IEnumerable<IServiceReference> GetServicesRegisteredBy(IModule module);

        IEnumerable<IServiceReference> GetServiceReferences(IModuleContainer container, Func<IServiceReference, bool> filter);

        IEnumerable<IServiceReference> GetServicesUsedBy(IModule module);

        void ReleaseServicesUsedBy(IModule module);

        object GetService(IModule module, IComponentRegistration componentRegistration);

        object GetService(IModule module, IServiceReference reference);

        bool UngetService(IModule module, IServiceReference reference, object service);

        void RaiseEvent(ServiceEvent eventObject);
    }
}
