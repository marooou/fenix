﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mni.Core;

namespace Fenix.Framework.Core.ServiceLayer
{
    internal sealed class ServiceUsage
    {
        public ServiceReference Reference { get; private set; }
        public int Count { get; private set; }
        public object Instance { get; set; }

        public ServiceUsage(ServiceReference reference)
        {
            this.Reference = reference;
        }

        public void Increment()
        {
            this.Count++;
        }

        public void Decrement()
        {
            this.Count--;
        }

        public void ResetCount()
        {
            this.Count = 0;
        }
    }
}
