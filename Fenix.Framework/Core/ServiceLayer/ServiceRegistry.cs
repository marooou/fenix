﻿namespace Fenix.Framework.Core.ServiceLayer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Fenix.Framework.Core;
    using Mni.Core;
    using System.Linq;
    using global::Autofac;
    using global::Autofac.Builder;
    using global::Autofac.Core.Registration;
    using global::Autofac.Core;

    using IModule = Mni.Core.IModule;

    internal class ServiceRegistry : ListenerRegistry<IServiceListener>, IServiceRegistry
    {
        private static readonly IDictionary<string, object> DefaultProperties = new Dictionary<string, object> 
        { 
            { Constants.Component.Activation, ComponentActivation.Eager },
            { Constants.Component.Scope, ComponentScope.Singleton }
        };

        private readonly IEventDispatcher _eventDispatcher;
        private readonly IFrameworkModule _framework;

        private readonly IDictionary<IModule, IList<ServiceRegistration>> _registrationsPerModule;
        private readonly IDictionary<IModule, IList<ServiceUsage>> _usages;
        private readonly IDictionary<Guid, ServiceRegistration> _registrations;

        public ServiceRegistry(IEventDispatcher eventDispatcher, IFrameworkModule framework)
        {
            this._eventDispatcher = eventDispatcher;
            this._framework = framework;
            this._registrationsPerModule = new Dictionary<IModule, IList<ServiceRegistration>>();
            this._registrations = new Dictionary<Guid, ServiceRegistration>();
            this._usages = new Dictionary<IModule, IList<ServiceUsage>>();
        }

        public void RaiseEvent(ServiceEvent eventObject)
        {
            this._eventDispatcher.Dispatch(this.GetListeners(), eventObject, this.SafelyNotifyListener);
        }

        public IServiceRegistration CreateRegistration(IModuleContainer scope, Type impl, Type[] ifs, IDictionary<string, object> properties)
        {
            var builder = new ContainerBuilder();

            IComponentRegistration componentRegistration = builder.RegisterType(impl)
                                                         .As(RegistrationKind.Service, ifs)
                                                         .WithSettings(DefaultProperties)
                                                         .WithSettings(properties)
                                                         .CreateRegistration()
                                                         .ManagedBy(scope);
            
            IList<ServiceRegistration> moduleRegistrations = null;
            if (!this._registrationsPerModule.TryGetValue(scope.Module, out moduleRegistrations))
            {
                moduleRegistrations = new List<ServiceRegistration>();
                this._registrationsPerModule.Add(scope.Module, moduleRegistrations);
            }

            var serviceRegistration = new ServiceRegistration(scope, componentRegistration, this);





            moduleRegistrations.Add(serviceRegistration);
            this._registrations.Add(componentRegistration.Id, serviceRegistration);

            if (serviceRegistration.Scope != ComponentScope.Transient && serviceRegistration.Activation == ComponentActivation.Eager)
            {
                this.GetService(scope.Module, serviceRegistration.GetReference());
            }

            var registeredEvent = new ServiceEvent(serviceRegistration.GetReference(), ServiceEventType.Registered);
            this.RaiseEvent(registeredEvent);
            return serviceRegistration;
        }

        public IEnumerable<IServiceRegistration> GetRegistrations()
        {
            return this._registrations.Values.ToArray();
        } 

        public object GetService(IModule module, IComponentRegistration componentRegistration)
        {
            ServiceRegistration reg;
            if (this._registrations.TryGetValue(componentRegistration.Id, out reg))
            {
                return this.GetService(module, reg.GetReference());
            }

            // TODO: log there is no such registration here
            return null;
        }
        
        public IEnumerable<IServiceRegistration> GetRegistrationsBy(IModule module)
        {
            if (module == null)
            {
                return this.GetRegistrations();
            }

            IList<ServiceRegistration> moduleRegistrations = null;
            if (this._registrationsPerModule.TryGetValue(module, out moduleRegistrations))
            {
                return moduleRegistrations.ToArray();
            }

            return Enumerable.Empty<IServiceRegistration>();
        }
        
        public IEnumerable<IServiceReference> GetServicesRegisteredBy(IModule module)
        {
            return this.GetRegistrationsBy(module).Select(r => r.GetReference());
        }

        public void ReleaseServicesUsedBy(IModule module)
        {
            IList<ServiceUsage> moduleUsages = null;
            if (this._usages.TryGetValue(module, out moduleUsages))
            {
                foreach (var serviceUsage in moduleUsages)
                {
                    serviceUsage.Reference.Registration.ReleaseService(module, serviceUsage.Instance);
                    moduleUsages.Remove(serviceUsage);
                }

                this._usages.Remove(module);
            }
        }

        public IEnumerable<IServiceReference> GetServicesUsedBy(IModule module)
        {
            if (this._usages.ContainsKey(module))
            {
                return this._usages[module].Where(u => u.Count > 0 && u.Instance != null)
                                           .Select(u => u.Reference).ToArray();
            }

            return Enumerable.Empty<IServiceReference>();
        }

        public void DestroyRegistration(IServiceRegistration registration)
        {
            var managedRef = (ServiceRegistration)registration;

            // Remove registration from available
            IModule provider = registration.GetReference().Provider;
            IList<ServiceRegistration> moduleRegistrations = null;
            bool found = false;
            if (this._registrationsPerModule.TryGetValue(provider, out moduleRegistrations))
            {
                if (this._registrations.ContainsKey(managedRef.Id))
                {
                    found = true;
                    moduleRegistrations.Remove(managedRef);
                    this._registrations.Remove(managedRef.Id);

                    if (moduleRegistrations.Count == 0)
                    {
                        this._registrationsPerModule.Remove(provider);
                    }
                }
            }

            if (!found)
            {
                throw new InvalidOperationException("The registration could not be found.");
            }

            // Clean up registration usages
            IServiceReference registrationRef = managedRef.GetReference();
            var eventObject = new ServiceEvent(registrationRef, ServiceEventType.Unregistering);
            this.RaiseEvent(eventObject);
            
            var emptyUsages = new List<IModule>();
            foreach (var module in this._usages.Keys)
            {
                IList<ServiceUsage> moduleUsages = this._usages[module];
                var usages = moduleUsages.Where(u => u.Reference.Equals(registrationRef)).ToArray();
                foreach (var serviceUsage in usages)
                {
                    managedRef.ReleaseService(module, serviceUsage.Instance);
                    moduleUsages.Remove(serviceUsage);
                }

                if (moduleUsages.Count == 0)
                {
                    emptyUsages.Add(module);
                }
            }

            foreach (var emptyUsage in emptyUsages)
            {
                this._usages.Remove(emptyUsage);
            }
        }

        public IEnumerable<IModule> GetUsingModules(IServiceRegistration registration)
        {
            var usingModules = new List<IModule>();
            foreach (IModule module in this._usages.Keys)
            {
                if (this._usages[module].Any(u => u.Reference.Registration.Equals(registration) && u.Count > 0))
                {
                    usingModules.Add(module);
                }
            }

            return usingModules;
        }

        public IEnumerable<IServiceReference> GetServiceReferences(IModuleContainer scope, Func<IServiceReference, bool> filter)
        {
            IEnumerable<IServiceReference> references = this._registrations.Values.Select(r => r.GetReference());
            if (filter != null)
            {
                return references.Where(filter);
            }

            return references;
        }

        public object GetService(IModule requestor, IServiceReference reference)
        {
            ServiceReference realReference = GetManagedReference(reference);
            if (!realReference.Registration.Valid)
            {
                return null;
            }

            IList<ServiceUsage> moduleUsages;
            ServiceUsage usage = null;
            if (this._usages.TryGetValue(requestor, out moduleUsages))
            {
                if (realReference.Registration.Scope != ComponentScope.Transient)
                {
                    usage = moduleUsages.SingleOrDefault(u => ReferenceEquals(u.Reference, realReference));
                }
            }
            else
            {
                moduleUsages = new List<ServiceUsage>();
                this._usages.Add(requestor, moduleUsages);
            }

            if (usage == null)
            {
                usage = new ServiceUsage(realReference);
                moduleUsages.Add(usage);
            }

            usage.Increment();
            if (usage.Instance == null)
            {
                usage.Instance = realReference.Registration.AcquireService(requestor);
            }

            return usage.Instance;
        }

        public bool UngetService(IModule module, IServiceReference reference, object service)
        {
            ServiceReference realReference = GetManagedReference(reference);
            IList<ServiceUsage> moduleUsages;
            ServiceUsage usage = null;
            if (this._usages.TryGetValue(module, out moduleUsages))
            {
                usage = moduleUsages.SingleOrDefault(u => u.Reference.Equals(realReference) && ReferenceEquals(u.Instance, service));
            }

            if (usage != null && usage.Count > 0)
            {
                usage.Decrement();
                if (usage.Count == 0)
                {
                    realReference.Registration.ReleaseService(module, service);
                    usage.Instance = null;
                    moduleUsages.Remove(usage);
                }

                if (moduleUsages.Count == 0)
                {
                    this._usages.Remove(module);
                }

                return true;
            }

            return false;
        }

        private void SafelyNotifyListener(ListenerInfo<IServiceListener> listenerInfo, ServiceEvent eventObject)
        {
            try
            {
                if (listenerInfo.Accepts(eventObject))
                {
                    listenerInfo.Listener.ServiceChanged(eventObject);
                }
            }
            catch (Exception ex)
            {
                var eventObj = new FrameworkEvent(listenerInfo.Provider, FrameworkEventType.Error, ex);
                this._framework.RaiseEvent(eventObj);
            }
        }

        private static ServiceReference GetManagedReference(IServiceReference reference)
        {
            var concreteRef = reference as ServiceReference;
            if (concreteRef == null)
            {
                throw new ArgumentException("Unmanaged service reference detected. Please get the reference using GetServiceReferences method.");
            }

            return concreteRef;
        }
    }
}
