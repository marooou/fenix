﻿namespace Fenix.Framework.Core.ServiceLayer
{
    using System;
    using System.Collections.Generic;

    using Mni.Core;

    using global::Autofac.Core.Registration;

    using IModule = Mni.Core.IModule;

    /// <summary>
    /// This is basically a wrapper for service registration not to be able to cast it to <see cref="ComponentRegistration"/>
    /// </summary>
    internal class ServiceReference : IServiceReference
    {
        internal ServiceRegistration Registration { get; private set; }

        public ServiceReference(ServiceRegistration registration)
        {
            if (registration == null) throw new ArgumentNullException("registration");
            this.Registration = registration;
        }

        public bool Equals(IServiceReference other)
        {
            return this.Equals((object)other);
        }

        public override bool Equals(object obj)
        {
            var serviceReg = obj as ServiceRegistration;
            if (serviceReg != null)
            {
                return this.Registration.Id == serviceReg.Id;
            }

            var serviceRef = obj as ServiceReference;
            if (serviceRef != null)
            {
                return this.Registration.Id == serviceRef.Registration.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.Registration.GetHashCode();
        }

        public IEnumerable<string> PropertyKeys
        {
            get { return this.Registration.PropertyKeys; }
        }

        public IModule Provider
        {
            get 
            { 
                return this.Registration.Valid ? this.Registration.Module : null; 
            }
        }

        public IEnumerable<IModule> GetUsingModules()
        {
            return this.Registration.GetUsingModules();
        }

        public object GetProperty(string key)
        {
            return this.Registration.GetProperty(key);
        }
    }
}
