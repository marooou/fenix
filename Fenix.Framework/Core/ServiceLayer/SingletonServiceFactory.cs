﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac.Core;
using Mni.Core;

namespace Fenix.Framework.Core.ServiceLayer
{
    using IModule = Mni.Core.IModule;

    internal class SingletonServiceFactory : IServiceFactory
    {
        private readonly IModuleContainer _owner;
        private readonly object _instanceSync;
        private volatile object _instance;

        public SingletonServiceFactory(IModuleContainer owner)
        {
            this._owner = owner;
            this._instanceSync = new object();
        }

        public object GetService(IModule module, IServiceRegistration registration)
        {
            if (this._instance == null)
            {
                lock (this._instanceSync)
                {
                    if (this._instance == null)
                    {
                        this._instance = this._owner.GetService(module, registration);
                    }
                }
            }

            return this._instance;
        }

        public void UngetService(IModule module, IServiceRegistration registration, object service)
        {
        }

        public void Destroy(IModule module, IServiceRegistration registration)
        {
            if (this._instance != null)
            {
                lock (this._instanceSync)
                {
                    if (this._instance != null)
                    {
                        this._owner.UngetService(module, registration, this._instance);
                        this._instance = null;
                    }
                }
            }
        }
    }
}
