﻿namespace Fenix.Framework.Core
{
    using System;
using Mni.Core;

    internal sealed class ListenerInfo<TListener>
    {
        public TListener Listener { get; private set; }
        public IModule Provider { get; private set; }
        public Func<EventArgs, bool> Filter { get; private set; }

        public ListenerInfo(TListener listener, IModule provider)
            : this(listener, provider, null)
        {
        }

        public ListenerInfo(TListener listener, IModule provider, Func<EventArgs, bool> filter)
        {
            this.Listener = listener;
            this.Provider = provider;
            this.Filter = filter;
        }

        public bool Accepts(EventArgs eventObject)
        {
            return this.Filter == null || this.Filter(eventObject);
        }
    }
}
