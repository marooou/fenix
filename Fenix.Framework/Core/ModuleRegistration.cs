﻿namespace Fenix.Framework.Core
{
    using Mni.Core;

    internal class ModuleRegistration : IModuleAware
    {
        public ModuleRegistration(IModule module)
        {
            this.Module = module;
        }

        public ModuleRegistration(IModuleContainer container)
        {
            this.Module = container.Module;
            this.Container = container;
        }

        public IModule Module { get; private set; }
        public IModuleContainer Container { get; set; }
    }
}
