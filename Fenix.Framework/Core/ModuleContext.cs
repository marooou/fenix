﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;

    using Autofac;

    using Mni.Core;

    internal class ModuleContext : IModuleContext
    {
        private readonly Module _module;

        public ModuleContext(Module module, IModuleContainer scope)
        {
            this._module = module;
            this.Scope = scope;
        }

        internal IModuleContainer Scope { get; private set; }

        public IModule Module
        {
            get
            {
                return this._module;
            }
        }

        public IModule GetModule(long id)
        {
            if (id == this.Module.Id)
            {
                return this.Module;
            }

            return this.Framework.ModuleRegistry.GetModule(id);
        }

        public IEnumerable<IModule> GetModules()
        {
            return this.Framework.ModuleRegistry.GetModules();
        }
                
        public IModule InstallModule(string path)
        {
            IModule module = this.Framework.ModuleInstaller.InstallModule(path);
            this.Framework.ModuleRegistry.Register(module);
            var installationEvent = new ModuleEvent(module, ModuleEventType.Installed, this.Module);
            this.Framework.ModuleRegistry.RaiseEvent(installationEvent);
            return module;
        }

        public object Resolve(Type type)
        {
            return this.Scope.Resolve(type);
        }

        public IServiceRegistration RegisterService(Type impl, Type[] ifs, IDictionary<string, object> properties)
        {
            return this.Framework.ServiceRegistry.CreateRegistration(this.Scope, impl, ifs, properties);
        }

        public IEnumerable<IServiceReference> GetServiceReferences(Func<IServiceReference, bool> filter)
        {
            return this.Framework.ServiceRegistry.GetServiceReferences(this.Scope, filter);
        }

        public object GetService(IServiceReference serviceReference)
        {
            return this.Framework.ServiceRegistry.GetService(this.Module, serviceReference);
        }

        public bool UngetService(IServiceReference serviceReference, object service)
        {
            return this.Framework.ServiceRegistry.UngetService(this.Module, serviceReference, service);
        }

        public void AddServiceListener(IServiceListener listener, Func<ServiceEvent, bool> filter)
        {
            this.Framework.ServiceRegistry.AddListener(this.Module, listener, CreateRawFilter(filter));
        }

        public void RemoveServiceListener(IServiceListener listener)
        {
            this.Framework.ServiceRegistry.RemoveListener(this.Module, listener);
        }

        public void AddModuleListener(IModuleListener listener, Func<ModuleEvent, bool> filter)
        {
            this.Framework.ModuleRegistry.AddListener(this.Module, listener, CreateRawFilter(filter));
        }

        public void RemoveModuleListener(IModuleListener listener)
        {
            this.Framework.ModuleRegistry.RemoveListener(this.Module, listener);
        }

        public void AddFrameworkListener(IFrameworkListener listener)
        {
            this.Framework.AddListener(this.Module, listener);
        }

        public void RemoveFrameworkListener(IFrameworkListener listener)
        {
            this.Framework.RemoveListener(this.Module, listener);
        }

        internal IFrameworkModule Framework
        {
            get
            {
                return this._module.Framework;
            }
        }

        private static Func<EventArgs, bool> CreateRawFilter<TEvent>(Func<TEvent, bool> filter) where TEvent : EventArgs
        {
            Func<EventArgs, bool> rawFilter = null;
            if (filter != null)
            {
                rawFilter = evt =>
                {
                    TEvent stronglyTypedEvt = evt as TEvent;
                    return stronglyTypedEvt != null && filter(stronglyTypedEvt);
                };
            }

            return rawFilter;
        }
    }
}
