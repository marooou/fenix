﻿namespace Fenix.Framework.Core
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using Mni.Core;

    internal class ModuleResolver : IModuleResolver
    {
        public Assembly[] Resolve(IModule module)
        {
            var assemblyPathCollection = module.FindEntries("bin", "*.dll", false);
            var assemblyList = new List<Assembly>();
            foreach (var assemblyPath in assemblyPathCollection)
            {
                var assembly = Assembly.LoadFile(Path.Combine(module.Location, assemblyPath));
                assemblyList.Add(assembly);
            }

            return assemblyList.ToArray();
        }


        public bool IsInUse(IModule module)
        {
            // TODO: check if
            return true;
        }
    }
}
