﻿namespace Fenix.Framework.Core
{
    using Fenix.Framework.Core.ServiceLayer;

    using Mni.Core;

    internal interface IFrameworkModule
    {
        IModuleContainer Container { get; }

        IModuleContainerRegistry ContainerRegistry { get; }
        IModuleRegistry ModuleRegistry { get; }
        IServiceRegistry ServiceRegistry { get; }
        IModuleResolver ModuleResolver { get; }
        IModuleInstaller ModuleInstaller { get; }

        void AddListener(IModule module, IFrameworkListener listener);
        void RemoveListener(IModule module, IFrameworkListener listener);
        void RaiseEvent(FrameworkEvent eventObject);
    }
}
