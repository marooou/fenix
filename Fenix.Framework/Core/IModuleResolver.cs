﻿namespace Fenix.Framework.Core
{
    using System.Reflection;

    using Mni.Core;

    internal interface IModuleResolver
    {
        Assembly[] Resolve(IModule module);
        bool IsInUse(IModule module);
    }
}
