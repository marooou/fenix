﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Fenix.Framework.Enterprise.Cdi;

    using Mni.Core;

    using global::Autofac;

    using IModule = Mni.Core.IModule;

    internal class ModuleRegistry : ListenerRegistry<IModuleListener>, IModuleRegistry, IModuleContainerRegistry
    {
        private readonly IFrameworkModule _framework;
        private readonly IEnumerable<IStereotypeHandler> _stereotypeHandlers;
        private readonly IEventDispatcher _eventDispatcher;

        private volatile IDictionary<long, ModuleRegistration> _registrations;
        private readonly object _registrationsLock;

        private readonly IListenerRegistry<IModuleListener> _listenerRegistry;

        public ModuleRegistry(IEnumerable<IStereotypeHandler> stereotypeHandlers, IFrameworkModule framework, IEventDispatcher eventDispatcher)
        {
            this._registrations = new Dictionary<long, ModuleRegistration>();
            this._registrationsLock = new object();
            this._framework = framework;
            this.Register(new ModuleRegistration(framework.Container));

            this._listenerRegistry = new ListenerRegistry<IModuleListener>();

            this._eventDispatcher = eventDispatcher;
            this._stereotypeHandlers = stereotypeHandlers;
        }

        public IModule GetModule(long id)
        {
            ModuleRegistration registration = this.GetRegistration(id);
            return registration.Module;
        }

        public IEnumerable<IModule> GetModules()
        {
            return this._registrations.Values.Select(x => x.Module).ToArray();
        }

        public void Register(IModule module)
        {
            this.Register(new ModuleRegistration(module));
        }

        public void Start(Module module)
        {
            ModuleRegistration registration = this.GetRegistration(module.Id);
            if (registration.Container != null)
            {
                throw new ModuleRegistryException(string.Format("Module {0} has already been started", module));
            }

            var builder = new ModuleContainerBuilder(module, this._stereotypeHandlers, this._framework.ServiceRegistry);
            TryRegisterActivator(module, builder);
            var container = builder.BuildModule();
            registration.Container = container;
            module.BindContext(container.Resolve<ModuleContext>());
        }

        public bool TryStartActivator(IModule module)
        {
            return this.TryProcessActivator(module, (activator, context) => activator.Start(context));
        }

        public void Stop(Module module)
        {
            ModuleRegistration registration = this.GetRegistration(module.Id);
            if (registration.Container == null)
            {
                throw new ModuleRegistryException(string.Format("Module {0} has not been stated", registration.Module));
            }

            module.UnbindContext();
            registration.Container.Dispose();
            registration.Container = null;
        }

        public bool TryStopActivator(IModule module)
        {
            return this.TryProcessActivator(module, (activator, context) => activator.Stop(context));
        }

        public bool Unregister(IModule module)
        {
            ModuleRegistration registration = this.GetRegistration(module.Id);
            if (registration.Container != null)
            {
                throw new ModuleRegistryException(string.Format("Module must be stopped before unregistering."));
            }

            if (module.Assemblies == null)
            {
                this._registrations.Remove(module.Id);
                return true;
            }

            return false;
        }

        public void RaiseEvent(ModuleEvent eventObject)
        {
            var allListeners = this.GetListeners().ToLookup(li => li.Listener is ISynchronousModuleListener);
            var syncListeners = allListeners[true];

            this._eventDispatcher.Dispatch(syncListeners, eventObject, this.SafelyNotifyListener);

            // There is no point in delivering transient event types asynchronously
            bool asyncEnabled = eventObject.Type != ModuleEventType.Starting && eventObject.Type != ModuleEventType.Stopping;
            if (asyncEnabled)
            {
                var listeners = allListeners[false];
                this._eventDispatcher.DispatchAsync(listeners, eventObject, this.SafelyNotifyListener);
            }
        }

        public IEnumerable<IModuleContainer> GetContainers(Func<IModuleContainer, bool> filter = null)
        {
            var containerRegs = this._registrations.Where(x => x.Value.Container != null);
            if (filter != null) 
            {
                containerRegs = containerRegs.Where(x => filter(x.Value.Container));
            }

            return containerRegs.Select(x => x.Value.Container).ToArray();
        }
        
        private static void TryRegisterActivator(IModule module, ModuleContainerBuilder builder)
        {
            string activatorTypeName = module.Manifest.Activator;
            if (string.IsNullOrEmpty(activatorTypeName))
            {
                return;
            }

            if (module.Assemblies == null)
            {
                throw new ModuleRegistryException(string.Format("Module {0} has unresolved assemblies", module));
            }

            Type activatorType = module.Assemblies.SelectMany(t => t.GetTypes()).FirstOrDefault(t => t.FullName.Equals(activatorTypeName));
            if (activatorType != null)
            {
                if (!typeof(IModuleActivator).IsAssignableFrom(activatorType))
                {
                    throw new ModuleRegistryException(
                        string.Format(
                            "Type {0} does not implement IModuleActivator interface in {1} module",
                            activatorType,
                            module));
                }

                builder.RegisterType(activatorType).As<IModuleActivator>().SingleInstance().AutoActivate();
            }
            else
            {
                throw new ModuleRegistryException(string.Format("Could not find '{0}' activator in {1} module assemblies", activatorTypeName, module));
            }
        }

        private bool TryProcessActivator(IModule module, Action<IModuleActivator, IModuleContext> action)
        {
            ModuleRegistration registration = this.GetRegistration(module.Id);
            var activator = registration.Container.ResolveOptional<IModuleActivator>();
            if (activator != null)
            {
                try
                {
                    action(activator, module.Context);
                    return true;
                }
                catch (Exception ex)
                {
                    throw new ModuleException(string.Format("Module {0} activator threw an exception", module), ex);
                }
            }

            return false;
        }
        
        private void Register(ModuleRegistration registration)
        {
            if (registration == null)
            {
                throw new ArgumentNullException("registration");
            }

            if (this._registrations.ContainsKey(registration.Module.Id))
            {
                throw new ModuleRegistryException(
                    string.Format("Module {0} has already been registered", registration.Module.Id));
            }

            // Use copy-on-write approach to reduce locking in read operations.
            var registrations = new Dictionary<long, ModuleRegistration>(this._registrations);
            registrations.Add(registration.Module.Id, registration);
            lock (this._registrationsLock)
            {
                this._registrations = registrations;
            }
        }

        private ModuleRegistration GetRegistration(long id)
        {
            ModuleRegistration registration;
            if (!this._registrations.TryGetValue(id, out registration))
            {
                throw new ModuleRegistryException(string.Format("Module id: {0} has not been registered", id));
            }

            return registration;
        }

        private void SafelyNotifyListener(ListenerInfo<IModuleListener> listenerInfo, ModuleEvent eventObject)
        {
            try
            {
                if (listenerInfo.Accepts(eventObject))
                {
                    listenerInfo.Listener.ModuleChanged(eventObject);
                }
            }
            catch (Exception ex)
            {
                var eventObj = new FrameworkEvent(listenerInfo.Provider, FrameworkEventType.Error, ex);
                this._framework.RaiseEvent(eventObj);
            }
        }
    }
}
