﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mni.Core;

namespace Fenix.Framework.Core
{
    internal class ListenerRegistry<TListener> : IListenerRegistry<TListener>
    {
        private readonly IDictionary<IModule, IList<ListenerInfo<TListener>>> _listeners;
        private readonly object _listenersLock;

        public ListenerRegistry()
        {
            this._listeners = new Dictionary<IModule, IList<ListenerInfo<TListener>>>();
            this._listenersLock = new object();
        }

        public void AddListener(IModule module, TListener listener, Func<EventArgs, bool> filter)
        {
            if (module == null) throw new ArgumentNullException("module");
            if (listener == null) throw new ArgumentNullException("listener");

            lock (this._listenersLock)
            {
                IList<ListenerInfo<TListener>> moduleListeners;
                if (!this._listeners.TryGetValue(module, out moduleListeners))
                {
                    moduleListeners = new List<ListenerInfo<TListener>>();
                    this._listeners.Add(module, moduleListeners);
                }

                var info = new ListenerInfo<TListener>(listener, module, filter);
                moduleListeners.Add(info);
            }
        }

        public void RemoveListener(IModule module, TListener listener)
        {
            if (module == null) throw new ArgumentNullException("module");
            if (listener == null) throw new ArgumentNullException("listener");

            lock (this._listenersLock)
            {
                IList<ListenerInfo<TListener>> moduleListeners;
                if (this._listeners.TryGetValue(module, out moduleListeners))
                {
                    ListenerInfo<TListener> toRemove = moduleListeners.FirstOrDefault(li => ReferenceEquals(li.Listener, listener));
                    if (toRemove != null)
                    {
                        moduleListeners.Remove(toRemove);
                        if (moduleListeners.Count == 0)
                        {
                            this._listeners.Remove(module);
                        }

                        return;
                    }
                }

                throw new InvalidOperationException("No such listener was registered");
            }
        }

        public bool RemoveAllListeners(IModule module)
        {
            if (module == null) throw new ArgumentNullException("module");

            lock (this._listenersLock)
            {
                return this._listeners.Remove(module);
            }
        }

        public IEnumerable<ListenerInfo<TListener>> GetListeners()
        {
            lock (this._listenersLock)
            {
                return this._listeners.SelectMany(g => g.Value).ToArray();
            }
        }
    }
}
