﻿namespace Fenix.Framework.Core
{
    using Mni.Core;

    internal interface IModuleManifestReader
    {
        IModuleManifest ReadManifest(string moduleDir);
    }
}
