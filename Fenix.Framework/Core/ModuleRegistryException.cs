﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Runtime.Serialization;

    internal class ModuleRegistryException : Exception
    {
        public ModuleRegistryException()
        {
        }

        public ModuleRegistryException(string message)
            : base(message)
        {
        }

        public ModuleRegistryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ModuleRegistryException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
