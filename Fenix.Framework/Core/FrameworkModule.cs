﻿namespace Fenix.Framework.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Reflection;
    using System.Threading;

    using Fenix.Framework.Core.ServiceLayer;
    using Fenix.Framework.Enterprise.Cdi;

    using Mni.Core;

    using global::Autofac;

    using Module = Fenix.Framework.Core.Module;

    internal sealed class FrameworkModule : Core.Module, IFramework, IFrameworkModule
    {
        private readonly IListenerRegistry<IFrameworkListener> _listenerRegistry;
        private ManualResetEvent _resetEvent;
        private IEventDispatcher _eventDispatcher;

        public IModuleContainer Container { get; private set; }
        public IModuleContainerRegistry ContainerRegistry { get; private set; }
        public IModuleRegistry ModuleRegistry { get; private set; }
        public IServiceRegistry ServiceRegistry { get; private set; }
        public IModuleResolver ModuleResolver { get; private set; }
        public IModuleInstaller ModuleInstaller { get; private set; }

        public FrameworkModule(IDictionary<string, string> configuration)
            : base(string.Empty)
        {
            this.Id = 0;
            this.Resolve();
            var executingAssembly = Assembly.GetExecutingAssembly();
            this.Manifest = this.CreateFrameworkManifest(executingAssembly, configuration);
            this._listenerRegistry = new ListenerRegistry<IFrameworkListener>();
        }
        
        public void Init()
        {
            if (this.Container != null)
            {
                return;
            }

            var builder = new ModuleContainerBuilder(this);
            this.Configure(builder);
            this.Container = builder.BuildModule(false);
            this.BindContext(this.Container.Resolve<ModuleContext>());
            this.ChangeState(ModuleState.Starting);
            this.ModuleRegistry = this.Container.Resolve<IModuleRegistry>();
            this.ServiceRegistry = this.Container.Resolve<IServiceRegistry>();
            this.ModuleResolver = this.Container.Resolve<IModuleResolver>();
            this.ModuleInstaller = this.Container.Resolve<IModuleInstaller>();
            this.ContainerRegistry = this.Container.Resolve<IModuleContainerRegistry>();
            this.InjectFramework(this);

            this._eventDispatcher = this.Container.Resolve<IEventDispatcher>();
            this._eventDispatcher.StartDispatching();

            this._resetEvent = new ManualResetEvent(false);
        }

        public override void Start()
        {
            switch (this.State)
            {
                case ModuleState.Uninstalled:
                    throw new InvalidOperationException("Cannot start module which is uninstalled.");
                case ModuleState.Starting:
                case ModuleState.Stopping:
                    throw new ModuleException(string.Format("Cannot start module which is {0}.", this.State.ToString().ToLower()));
                case ModuleState.Active:
                    return;
            }

            this.Init();

            // TODO: register all framework services

            this.ChangeState(ModuleState.Active);

            // TODO: start all modules that have autostart property
        }

        public override void Stop()
        {
            switch (this.State)
            {
                case ModuleState.Starting:
                case ModuleState.Stopping:
                    throw new ModuleException(string.Format("Cannot stop the framework which is {0}.", this.State.ToString().ToLower()));
                case ModuleState.Active:
                    Task.Factory.StartNew(this.Shutdown);
                    return;
                case ModuleState.Installed:
                case ModuleState.Resolved:
                    return;
                default:
                    throw new InvalidOperationException(string.Format("Invalid framework state: {0}"));
            }
        }

        public override void Uninstall()
        {
            throw new InvalidOperationException("The framework cannot be uninstalled");
        }

        public void WaitForStop(int millisecondsTimeout = -1)
        {
            if (this.IsInState(ModuleState.Starting | ModuleState.Stopping | ModuleState.Active))
            {
                this._resetEvent.WaitOne(millisecondsTimeout);
            }
        }

        public override IEnumerable<IServiceReference> GetRegisteredServices()
        {
            // This fallback is due to deferred service registry instantiation
            if (this.ServiceRegistry != null)
            {
                return base.GetRegisteredServices();
            }

            return Enumerable.Empty<IServiceReference>();
        }

        public override IEnumerable<IServiceReference> GetServicesInUse()
        {
            // This fallback is due to deferred service registry instantiation
            if (this.ServiceRegistry != null)
            {
                return base.GetServicesInUse();
            }

            return Enumerable.Empty<IServiceReference>();
        }

        public void AddListener(IModule module, IFrameworkListener listener)
        {
            this._listenerRegistry.AddListener(module, listener, null);
        }

        public void RemoveListener(IModule module, IFrameworkListener listener)
        {
            this._listenerRegistry.RemoveListener(module, listener);
        }

        public void RaiseEvent(FrameworkEvent eventObject)
        {
            this._eventDispatcher.DispatchAsync(this._listenerRegistry.GetListeners(), eventObject, this.SafelyNotifyListener);
        }

        protected override void Resolve()
        {
            this.Assemblies = new[] { Assembly.GetExecutingAssembly() };
            this.ChangeState(ModuleState.Resolved);
        }

        private void Configure(ContainerBuilder builder)
        {
            builder.RegisterInstance(this).As(typeof(IFrameworkModule), typeof(IFramework)).SingleInstance();
            builder.RegisterType<ModuleRegistry>().As(typeof(IModuleRegistry), typeof(IModuleContainerRegistry)).SingleInstance();
            builder.RegisterType<ServiceRegistry>().As<IServiceRegistry>().SingleInstance();
            builder.RegisterType<ModuleInstaller>().As<IModuleInstaller>().SingleInstance();
            builder.RegisterType<ModuleManifestReader>().As<IModuleManifestReader>().SingleInstance();
            builder.RegisterType<ModuleResolver>().As<IModuleResolver>().SingleInstance();
            builder.RegisterType<EventDispatcher>().As<IEventDispatcher>().SingleInstance();
            builder.RegisterType<ServiceStereotypeHandler>().As<IStereotypeHandler>().SingleInstance();
            builder.RegisterType<DependencyStereotypeHandler>().As<IStereotypeHandler>().SingleInstance();
        }

        private IModuleManifest CreateFrameworkManifest(Assembly executingAssembly, IDictionary<string, string> configuration)
        {
            var assemblyName = executingAssembly.GetName();
            IDictionary<string, string> props = new Dictionary<string, string>
            {
                { Constants.Module.SymbolicName, assemblyName.Name },
                { Constants.Module.Version, assemblyName.Version.ToString(3) },
                { Constants.Module.FrameworkGuid, Guid.NewGuid().ToString() }
            };

            if (configuration != null)
            {
                foreach (var configItem in configuration.Where(configItem => !props.ContainsKey(configItem.Key)))
                {
                    props.Add(configItem);
                }
            }

            return new ModuleManifest(props);
        }

        private void Shutdown()
        {
            this.ChangeState(ModuleState.Stopping);
            if (this.ModuleRegistry != null)
            {
                this.Notify(ModuleEventType.Stopping);
            }

            this.StopAllModules();
            this.ReleaseRegisteredServices();

            // This call is blocking; waiting until all events are delivered.
            this._eventDispatcher.StopDispatching();
            this.ChangeState(ModuleState.Resolved);
            this.UnbindContext();
            this._resetEvent.Set();
        }

        private void StopAllModules()
        {
            var modules = this.Context.GetModules().Where(m => m.Id != 0);
            foreach (var module in modules)
            {
                try
                {
                    module.Stop();
                }
                catch (Exception ex)
                {
                    var exception = new ModuleException(string.Format("Unable to stop {0} module", module), ex);
                    var errorEvent = new FrameworkEvent(module, FrameworkEventType.Error, exception);
                    this.RaiseEvent(errorEvent);
                }
            }
        }

        private void SafelyNotifyListener(ListenerInfo<IFrameworkListener> listenerInfo, FrameworkEvent eventObject)
        {
            try
            {
                listenerInfo.Listener.FrameworkEvent(eventObject);
            }
            catch (Exception ex)
            {
                // TODO: log error
            }
        }
    }
}
