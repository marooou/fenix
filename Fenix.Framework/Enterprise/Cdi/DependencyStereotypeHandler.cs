﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Framework.Enterprise.Cdi
{
    using Fenix.Framework.Core;

    using Mni.Core;
    using Mni.Enterprise.Cdi;
    using Autofac;
    using Autofac.Builder;

    internal class DependencyStereotypeHandler : IPrebuildStereotypeHandler
    {
        public bool PreBuild(ModuleContainerBuilder containerBuilder, Type type)
        {
            if (!type.IsClass || type.IsAbstract)
            {
                return false;
            }

            var dependencyAttribute = type.GetAttributes<DependencyAttribute>().SingleOrDefault();
            if (dependencyAttribute != null)
            {
                IDictionary<string, object> props = dependencyAttribute.ToDictionary();
                props.Update(type.GetAttributes<PropertyAttribute>().ToDictionary(), new[] { Constants.Component.ObjectClass });

                containerBuilder.RegisterType(type)
                                .As(RegistrationKind.Dependency, dependencyAttribute.Types)
                                .WithSettings(props);
                return true;
            }

            return false;
        }
    }
}
