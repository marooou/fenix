﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Framework.Enterprise.Cdi
{
    using Fenix.Framework.Core;
    using Fenix.Framework.Core.ServiceLayer;

    using Mni.Core;
    using Mni.Enterprise.Cdi;

    using Autofac;
    using Autofac.Core;

    internal class ServiceStereotypeHandler : IPostbuildStereotypeHandler
    {
        private readonly IServiceRegistry _serviceRegistry;

        public ServiceStereotypeHandler(IServiceRegistry serviceRegistry)
        {
            this._serviceRegistry = serviceRegistry;
        }

        public bool PostBuild(IModuleContainer container, Type type)
        {
            if (!type.IsClass || type.IsAbstract)
            {
                return false;
            }

            var serviceAttr = type.GetAttributes<ServiceAttribute>().SingleOrDefault();
            if (serviceAttr != null)
            {
                IDictionary<string, object> props = serviceAttr.ToDictionary();
                props.Update(type.GetAttributes<PropertyAttribute>().ToDictionary(), new[] { Constants.Component.ObjectClass });
                this._serviceRegistry.CreateRegistration(container, type, serviceAttr.Types, props);
                return true;
            }

            return false;
        }
    }
}
