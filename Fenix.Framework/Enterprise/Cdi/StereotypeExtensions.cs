﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Framework.Enterprise.Cdi
{
    using Mni.Enterprise.Cdi;

    internal static class StereotypeExtensions
    {
        public static IEnumerable<TAttr> GetAttributes<TAttr>(this Type type, bool inherit = true)
        {
            return type.GetCustomAttributes(typeof(TAttr), inherit).Cast<TAttr>();
        }

        public static IDictionary<string, object> ToDictionary(this IEnumerable<PropertyAttribute> properties)
        {
            return properties.ToDictionary(propertyAttribute => propertyAttribute.Key, propertyAttribute => propertyAttribute.Value);
        }
    }
}
