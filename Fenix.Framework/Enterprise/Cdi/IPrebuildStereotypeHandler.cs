﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Framework.Enterprise.Cdi
{
    using Fenix.Framework.Core;

    interface IPrebuildStereotypeHandler : IStereotypeHandler
    {
        bool PreBuild(ModuleContainerBuilder builder, Type type);
    }
}
