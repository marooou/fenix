﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenix.Framework.Enterprise.Cdi
{
    using Fenix.Framework.Core;

    interface IPostbuildStereotypeHandler : IStereotypeHandler
    {
        bool PostBuild(IModuleContainer container, Type type);
    }
}
